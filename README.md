![socib](http://socib.es/stylesheets/images/logo-Socib.png)

# socib_modelling ![bash](https://img.shields.io/badge/build-unknown-lightgrey)

![plataform](https://img.shields.io/badge/platform-linux--64%20%7C%20%20osx--64%20-lightgrey)

![python](https://img.shields.io/badge/python3-v3.6.8-brightgreen)

![license](https://img.shields.io/badge/license-MIT-green)

## Package of scripts, tools, and libraries commonly used at SOCIB Modelling and Forecasting Facility (SOCIB-MFF).

This suite provides access to a group of functionalities commonly applied in SOCIB-MFF daily tasks.
These functionalities include:
- **io**: Read/Write files of different formats like NetCDF, Grib, CSV, etc.
- **transform**: Transformations/conversions of datasets like crop, smoothing, etc
- **compute**: Compute new datasets or variables like compute_rmsd, compute_mae, compute_eke, etc
- **plot**: Plotting of time-series, maps, graphics in general.
- **security**: Control of credentials to access external services using login/password, token, API keys, etc.
- **utils**: Collection of small scripts and functions commonly used in all package.
- **external**: External packages like third-party libraries.

### Dependencies

* python3.7 or 3.8

#### Installing dependencies:
We recommend using a new python environment to install all dependencies necessaries.  
The environment.yml contains all information necessary to create a new environment using conda:

`$ conda env create -f environment.yml`

it will create an environment called mff. After creation, you can activate the environment using:

`$ conda activate mff`

and to deactivate an active environment, use:

`$ conda deactivate `


## Architecture

![Package's Architecture][arch]

[arch]: images/architecture.svg "Package's Architecture"
