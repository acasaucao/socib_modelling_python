# -*- coding: UTF-8 -*-

"""This module has functions to mine data sources (thredds)."""

import datetime
import os
import requests as rq
from bs4 import BeautifulSoup
from memoization import cached

from ..external.thredds_crawler.crawl import Crawl
from .. import io as mff_io
from .. import utils as mff_utils
from ..config import Config as MFF_Config

_MFF_CONFIG = MFF_Config()

MY_PATH = os.path.dirname(__file__)


class Miner(object):
    """
    Miner object to download remote products.

    Parameters
    ----------
    config_file : str
       The path for a configuration file describing the products.
    """

    def __init__(self):
        """Initialize from a config file."""
        super(Miner, self).__init__()
        self.config = _MFF_CONFIG.get("miner")

    # @cached(max_size=None)
    def crawl_remote_location(
        self, channel="opendap", product_name="wmop", date=None, crawler_kwargs={}
    ):
        """
        Get a remote location of a dataset using a crawler (thredds_crawler).

        Parameters
        ----------
        channel : str
           The channel used to download the dataset, currently only opendap is available.
        product_name : str
           Product name, e.g: wmop, sapo_ib, wmop_3d, ...
        date : str
           Date string indicating the specific date to be downloaded.
           The data format should be accepted by mff_utils.patterns's parser.
        crawler_kwargs : dict
           A dictionary with parameters for the thredds crawler. It can include select, skip,
           before, after, worker, etc.

        Returns
        -------
        file_location : str.
           Returns a string with a remote location.
        """
        product_info = self.config.get("products").get(product_name)
        thredds_info = product_info["repository"].get("thredds", {})
        remote_catalog = thredds_info.get('catalog').format(**thredds_info)
        remote_base = thredds_info.get("server")

        datasets = Crawl(remote_catalog, **crawler_kwargs).datasets

        file_location = [f"{remote_base}/dodsC/{ds.id}" for ds in datasets]
        if isinstance(file_location, list) and mff_utils.patterns.is_valid_date(date):
            parsed_date = mff_utils.patterns.parse_date(date)
            file_location = find_date_in_remote_files(parsed_date, file_location)

        return file_location

    @cached(max_size=None)
    def mine_product(
        self,
        channel="opendap",
        product_name="wmop",
        date="now",
        include=None,
        select=None,
        skip=None,
        select_strategy="first",
        crawler_kwargs={},
    ):
        """
        Read a remote dataset from databases.

        Parameters
        ----------
        channel : str
           The channel used to download the dataset, currently only opendap is available.
        product_name : str
           Product name, e.g: wmop, sapo_ib, wmop_3d, ...
        date : str
           Date string indicating the specific date to be downloaded.
           The data format should be accepted by mff_utils.patterns's parser.
           The value 'latest' indicates the latest file available.
        include : list of str
           List containing substrings that should be present in the remote location's string.
           The values are formmated to be included in 'select' list following
           the pattern '"(?=*.value)"'
        select : list of str
           List of strings with regular expressions used to filter files found by the crawler.
           More info at: https://github.com/ioos/thredds_crawler
        skip : list of str
           List of strings with regular expressions used to exclude files found by the crawler.
           More info at: https://github.com/ioos/thredds_crawler
        select_strategy : str
           Selection strategy when more than one remote nc file fills the criteria.
           the option are:
               'first': Get only the data from the first file that fills the criteria,
                        returns one Xarray's dataset
               'all': Get all the data from all files that fill the criteria,
                      returns a list of Xarray's dataset
        crawler_kwargs : dict
           A dictionary with parameters for the thredds crawler. It can include select, skip,
           before, after, worker, etc.

        Returns
        -------
        data : Xarray's dataset or list of Xarray's dataset.
           Returns Xarray's dataset or list of Xarray's dataset depending on the select_strategy.
        """
        if include is None and date == "latest":
            include = ["latest"]
        elif include is not None and date == "latest":
            include.append("latest")

        if include is not None and isinstance(include, list):
            if select is None:
                select = [f"(?=.*{pattern})" for pattern in include]
            else:
                select = select + [f"(?=.*{pattern})" for pattern in include]

        if skip is not None:
            skip = Crawl.SKIPS + skip

        crawler_kwargs["select"] = select
        crawler_kwargs["skip"] = skip
        file_locations = self.crawl_remote_location(
            channel=channel,
            product_name=product_name,
            date=date,
            crawler_kwargs=crawler_kwargs,
        )
        data = None
        if not isinstance(file_locations, list):
            data = mff_io.read(file_locations)
        elif select_strategy == "first":
            if len(file_locations) > 0:
                data = mff_io.read(file_locations[0])
        else:
            data = []
            for filename in file_locations:
                data.append(mff_io.read(filename))

        return data

    def list_products(self):
        """
        List all available products described for the configuration specified for the Miner.

        Returns
        -------
        products : list of str.
           Returns a list (str) of products.
        """
        return self.config.get("products").keys()

    def info_product(self, product_name):
        """
        Provide the characteristics of a product.

        Returns
        -------
        characteristics : dict
           Returns a dictionary with the product's characteristics.
        """
        characteristics = (
            self.config.get("products").get(product_name).get("characteristics", {})
        )
        return characteristics


def _get_info_nc(html):
    html = html.rstrip("\n")
    soup = BeautifulSoup(html, features="lxml")
    info = {}
    for trs in soup.findAll("tr"):
        tds = trs.findAll("td")
        k = tds[0].text
        v = tds[1].text
        if k[-1] == ":":
            k = k[:-1]
        info[k] = v
    return info


def _get_time_coverage(remote):
    start = None
    end = None
    try:
        request = rq.get("{}.info".format(remote))
        if request.status_code != 200:
            return start, end

        info = _get_info_nc(request.text)
        if "time_coverage_start" in info:
            start = mff_utils.patterns.parse_date(info["time_coverage_start"])
            start = start.replace(hour=0, minute=0, second=0)
            end = mff_utils.patterns.parse_date(info["time_coverage_end"])
        elif "date_created" in info:
            start = mff_utils.patterns.parse_date(info["date_created"])
            start = start.replace(hour=0, minute=0, second=0)
            end = start + datetime.timedelta(days=1)
    except Exception:
        raise

    return start, end


def _remote_has_date(date, remote):
    try:
        start, end = _get_time_coverage(remote)
        if (
            start is None and end is None
        ):  # no metadata of time was found, including dateset.
            return True
        elif date >= start and date <= end:
            return True
        return False
    except Exception:
        raise


def get_metadata_remote_file(remote):
    """Get the remote file metadata."""
    request = rq.get("{}.info".format(remote))
    if request.status_code != 200:
        return {}
    info = _get_info_nc(request.text)

    return info


def find_date_in_remote_files(date, remote_files):
    """Select remote files covering the date period, based on information from the metadata."""
    valid = []
    for remote in remote_files:
        if _remote_has_date(date, remote):
            valid.append(remote)

    if len(valid) == 1:
        valid = valid[0]

    return valid


def find_latest_in_remote_files(remote_files):
    """Get the remote file with the latest data, based on information from the metadata."""
    latest_data = None
    latest_date = None
    for remote in remote_files:
        start, end = _get_time_coverage(remote)
        if start is None and end is None:
            continue
        if latest_date is None:
            latest_data = remote
            latest_date = end
        elif end > latest_date:
            latest_data = remote
            latest_date = end

    return latest_data


def latest_wmop_3d():
    """Return the xarray dataset with the latest wmop 3d."""
    miner = Miner()
    data = miner.mine_product(product_name="wmop_3d", date="latest")
    return data


def get_glider_missions(date=None, crawler_kwargs={}):
    """
    Return the list of glider mission available on SOCIB's thredds.

    Parameters
    ----------
    date : str
       Date string indicating the specific date to be downloaded.
       The data format should be accepted by mff_utils.patterns's parser.
       The value 'latest' indicates the latest file available.
    crawler_kwargs : dict
       A dictionary with parameters for the thredds crawler. It can include select, skip, before,
       after, worker, etc.

    Returns
    -------
    glider_missions :str or list of str.
       Returns the glider missions remote files matching the date and crawler arguments.
       If date is equal 'latest' only one remote file is returned.
    """
    miner = Miner()
    latest = False
    if date == "latest":
        latest = True
        date = None

    glider_missions = miner.crawl_remote_location(
        channel="opendap",
        product_name="glider",
        date=date,
        crawler_kwargs=crawler_kwargs,
    )

    if latest:
        glider_missions = find_latest_in_remote_files(glider_missions)

    return glider_missions
