# -*- coding: UTF-8 -*-

"""Module to plot different types of maps."""

import numpy as np
import os

import matplotlib

if os.environ.get("DISPLAY", "") == "":
    print("no display found. Using non-interactive Agg backend")
    matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.ticker as mticker
from matplotlib.offsetbox import AnchoredText
import seaborn as sns

import cartopy.crs as ccrs
import cartopy
import cartopy.feature as cfeature
import cartopy.geodesic as geodesic
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import shapely
import gc

from ..config import Config as mff_Config

_MFF_CONFIG = mff_Config()


def plot_points_map(
    x,
    y,
    hue=None,
    ax=None,
    hue_order=None,
    style=None,
    style_order=None,
    cmap=cm.jet,
    colorbar=False,
    vmin=None,
    vmax=None,
    region="WesternMed",
    title="",
    output=None,
    coverage=False,
    emodnet=False,
    clean=True,
    show=True,
    kind="scatter",
    plot_empty=False,
    cb_diverging=False,
    cb_label="",
    cb_format="%0.2f",
    marker_size=100,
    fontsizes={"large": 20, "x-large": 24, "xx-large": 28},
):
    """Plot points in a map using Seaborn."""
    mpl_rc_params = plt.rcParams
    plt.rcParams["font.weight"] = "bold"

    cfg = _MFF_CONFIG.get("regions")["Custom"]
    if isinstance(region, str):
        cfg = _MFF_CONFIG.get("regions")[region]
    elif isinstance(region, dict):
        cfg.update(region)

    base_obs_palette = sns.color_palette("bright")
    base_coverage_palette = sns.color_palette("bright")

    if hue is not None:
        if hue.dtype.name == "object":
            categories = np.unique(hue)
            obs_palette = {}
            coverage_palette = {}
            for i, category in enumerate(categories):
                coverage_palette[category] = base_coverage_palette[i]
                obs_palette[category] = base_obs_palette[i]
        else:
            coverage_palette = None
            obs_palette = cmap
    else:
        coverage_palette = None
        obs_palette = cmap

    map_min_lon, map_max_lon, map_min_lat, map_max_lat = cfg["area"]

    filter_lon_lat = (
        (x >= map_min_lon)
        & (x <= map_max_lon)
        & (y >= map_min_lat)
        & (y <= map_max_lat)
    )

    x = x[filter_lon_lat].copy()
    y = y[filter_lon_lat].copy()
    if hue is not None:
        hue = hue[filter_lon_lat].copy()

    if (np.size(x) == 0) or (np.size(y) == 0):
        gc.collect()

        if not plot_empty:
            matplotlib.rc_file_defaults()
            return

        fig = plt.figure(**cfg["figure"])
        ax = plt.axes(projection=ccrs.PlateCarree())
        land = cfeature.NaturalEarthFeature(
            "physical",
            "land",
            "10m",
            edgecolor="face",
            facecolor=cfeature.COLORS["land"],
        )
        ax.add_feature(land)
        ax.coastlines(resolution="10m")
        ax.set_extent(
            [map_min_lon, map_max_lon, map_min_lat, map_max_lat], crs=ccrs.PlateCarree()
        )
        at = AnchoredText(
            "No data available",
            prop=dict(size=fontsizes["xx-large"], color="r"),
            frameon=True,
            loc="center",
        )
        at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        ax.add_artist(at)
        if title is None:
            title = "Coverage Map"
    else:
        if ax is None:
            fig = plt.figure(**cfg["figure"])
            ax = plt.axes(projection=ccrs.PlateCarree())
        ax.set_extent(
            [map_min_lon, map_max_lon, map_min_lat, map_max_lat], crs=ccrs.PlateCarree()
        )
        if emodnet:
            ax.add_wms(wms='http://ows.emodnet-bathymetry.eu/wms',
                       layers=['emodnet:mean_atlas_land', 'coastlines'])
        else:
            land = cfeature.NaturalEarthFeature(
               "physical",
               "land",
               "10m",
               edgecolor="face",
               facecolor=cfeature.COLORS["land"],
            )
            ax.add_feature(land)
            ax.coastlines(resolution="10m")

        if (vmin is None) and (hue is not None):
            vmin = np.nanmin(hue)
        if (vmax is None) and (hue is not None):
            vmax = np.nanmax(hue)

        if kind == "scatter":
            norm = None
            levels = None
            if hue is not None:
                levels = np.linspace(vmin, vmax, 100)
                if cb_diverging:
                    adj_vmin = min(-1 * vmax, vmin)
                    adj_vmax = max(abs(adj_vmin), vmax)
                    vmin = adj_vmin
                    vmax = adj_vmax
                    cmap = cm.RdBu_r
                    norm = matplotlib.colors.TwoSlopeNorm(
                        vmin=vmin, vcenter=0, vmax=vmax
                    )
            cs = plt.scatter(
                x, y, c=hue, s=marker_size, vmin=vmin, vmax=vmax, norm=norm, cmap=cmap
            )

            # legend formatting
            if colorbar:
                kwargs = {"format": cb_format}
                cb = plt.colorbar(
                    cs, orientation="vertical", pad=0.025, shrink=0.6, **kwargs
                )
                cb.set_label(cb_label, size=fontsizes["large"], weight="bold")
                cb.ax.tick_params(labelsize=fontsizes["large"])

        elif kind == "contour":
            norm = None
            levels = None
            if hue is not None:
                levels = np.linspace(vmin, vmax, 100)
                if cb_diverging:
                    adj_vmin = min(-1 * vmax, vmin)
                    adj_vmax = max(abs(adj_vmin), vmax)
                    vmin = adj_vmin
                    vmax = adj_vmax
                    cmap = cm.RdBu_r
                    norm = matplotlib.colors.DivergingNorm(
                        vmin=vmin, vcenter=0, vmax=vmax
                    )
            cs = plt.tricontourf(
                x,
                y,
                hue,
                vmin=vmin,
                vmax=vmax,
                levels=levels,
                norm=norm,
                cmap=cmap,
                extend="both",
            )
            if colorbar:
                kwargs = {"format": cb_format}
                cb = plt.colorbar(
                    cs, orientation="vertical", pad=0.025, shrink=0.6, **kwargs
                )
                cb.set_label(cb_label, size=fontsizes["large"], weight="bold")
                cb.ax.tick_params(labelsize=fontsizes["large"])

        if coverage:
            for idx in range(len(x)):
                lon = x[idx]
                lat = y[idx]
                color = "k"
                if hue is not None:
                    color = coverage_palette[hue[idx]]

                circle_points = geodesic.Geodesic().circle(
                    lon=lon, lat=lat, radius=200000, n_samples=100, endpoint=False
                )
                geom = shapely.geometry.Polygon(circle_points)
                ax.add_geometries(
                    (geom,),
                    crs=cartopy.crs.PlateCarree(),
                    facecolor=color,
                    edgecolor="none",
                    alpha=0.01,
                    linewidth=0,
                    zorder=0,
                )

    # gridlines formatting
    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=2,
        color="gray",
        alpha=0.5,
        linestyle="--",
    )
    if "xgridlines" in cfg:
        gl.xlocator = mticker.FixedLocator(cfg["xgridlines"])
    if "ygridlines" in cfg:
        gl.ylocator = mticker.FixedLocator(cfg["ygridlines"])
    gl.top_labels = False
    gl.right_labels = False
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {"color": "k", "weight": "bold", "size": fontsizes["xx-large"]}
    gl.ylabel_style = {"color": "k", "weight": "bold", "size": fontsizes["xx-large"]}

    # title and output formatting
    plt.title(title, size=fontsizes["xx-large"], weight="bold")
    if output is not None:
        fig.canvas.draw()
        plt.tight_layout()
        plt.savefig(output, dpi=300, bbox_inches="tight")

    if show:
        plt.show()

    try:
        if clean:
            plt.close("all")
            del ax, gl, land, fig
        del x, y, hue
    except:  # noqa
        pass
    _ = gc.collect()
    matplotlib.rcParams.update(mpl_rc_params)
    return


# -------------------------------------------------------------------------------
