# -*- coding: UTF-8 -*-

"""Module to apply grid transformations."""

from ..config import Config as MFF_Config

_MFF_CONFIG = MFF_Config()


def crop_grid(grid, area, coord_dict={}):
    """
    Crop a dataset based on the limits.

    Parameters
    ----------
    grid : xarray Dataset
       The geographic data to be croped.
    limits : tuple of floats
       Tuples of floats indicating (minimal x-coordination, maximum x-coordination,
                                    minimal y-coordination, maximum y-coordination)
    coord_dict: dict
       Dictionary defining the grid coordinates x, y.
       Default values are x => lon_rho and y => lat_rho
    """
    if isinstance(area, tuple):
        limits = area
        return crop_grid_by_limits(grid, limits, coord_dict=coord_dict)
    elif isinstance(area, str):
        limits = _MFF_CONFIG.get("regions").get(area)["area"]
        return crop_grid_by_limits(grid, limits, coord_dict=coord_dict)


def crop_grid_by_limits(grid, limits, coord_dict={}):
    """
    Crop a dataset based on the limits.

    Parameters
    ----------
    grid : xarray Dataset
       The geographic data to be croped.
    limits : tuple or list of floats
       Floats indicating (minimal x-coordination, maximum x-coordination,
                          minimal y-coordination, maximum y-coordination)
    coord_dict: dict
       Dictionary defining the grid coordinates x, y.
       Default values are x => lon_rho and y => lat_rho
    """
    x_coord = coord_dict.get("x", "lon_rho")
    y_coord = coord_dict.get("y", "lat_rho")

    crop_cmd = {
        x_coord: slice(limits[0], limits[1]),
        y_coord: slice(limits[2], limits[3]),
    }

    return grid.sel(**crop_cmd)
