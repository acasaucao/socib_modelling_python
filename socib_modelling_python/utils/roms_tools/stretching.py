import numpy as np


def stretching(Vstretching, theta_s, theta_b, N, kgrid, report=False):
    """
    function [s,C]=stretching(Vstretching, theta_s, theta_b, hc, N, kgrid, ...
                              report);

    %
    % STRETCHING:  Compute ROMS vertical coordinate stretching function
    %
    % [s,C]=stretching(Vstretching, theta_s, theta_b, hc, N, kgrid, report)
    %
    % Given vertical terrain-following vertical stretching parameters, this
    % this routine computes the vertical stretching function used used in
    % ROMS vertical coordinate transformation. Check the following link for
    % details:
    %
    %    https://www.myroms.org/wiki/index.php/Vertical_S-coordinate
    %
    % On Input:
    %
    %    Vstretching   Vertical stretching function:
    %                    Vstretching = 1,  original (Song and Haidvogel, 1994)
    %                    Vstretching = 2,  A. Shchepetkin (UCLA-ROMS, 2005)
    %                    Vstretching = 3,  R. Geyer BBL refinement
    %                    Vstretching = 4,  A. Shchepetkin (UCLA-ROMS, 2010)
    %    theta_s       S-coordinate surface control parameter (scalar)
    %    theta_b       S-coordinate bottom control parameter (scalar)
    %    N             Number of vertical levels (scalar)
    %    kgrid         Depth grid type logical switch:
    %                    kgrid = 0,        function at vertical RHO-points
    %                    kgrid = 1,        function at vertical W-points
    %    report        Flag to report detailed information (OPTIONAL):
    %                    report = 0,       do not report
    %                    report = 1,       report information
    %
    % On Output:
    %
    %    s             S-coordinate independent variable, [-1 <= s <= 0] at
    %                    vertical RHO- or W-points (vector)
    %    C             Nondimensional, monotonic, vertical stretching function,
    %                    C(s), 1D array, [-1 <= C(s) <= 0]
    %

    % svn $Id: stretching.m 544 2011-04-01 23:51:17Z arango $
    %=========================================================================%
    %  Copyright (c) 2002-2011 The ROMS/TOMS Group                            %
    %    Licensed under a MIT/X style license                                 %
    %    See License_ROMS.txt                           Hernan G. Arango      %
    %=========================================================================%
    """

    s = []
    C = []

    if (Vstretching < 1) or (Vstretching > 4):
        print(f"*** Error:  SET_DEPTH - Illegal parameter Vstretching = {Vstretching}")
        return None

    Np = N + 1

    # ----------------------------------------------------------------------------
    # Compute ROMS S-coordinates vertical stretching function
    # ----------------------------------------------------------------------------

    # Original vertical stretching function (Song and Haidvogel, 1994).

    if Vstretching == 1:
        ds = 1.0 / N
        if kgrid == 1:
            Nlev = Np
            lev = np.arange(0, N + 1)
            s = (lev - N) * ds
        else:
            Nlev = N
            lev = np.arange(1, N + 1) - 0.5
            s = (lev - N) * ds

        if theta_s > 0:
            Ptheta = np.sinh(theta_s * s) / np.sinh(theta_s)
            Rtheta = np.tanh(theta_s * (s + 0.5)) / (2.0 * np.tanh(0.5 * theta_s)) - 0.5
            C = (1.0 - theta_b) * Ptheta + theta_b * Rtheta
        else:
            C = s

    elif Vstretching == 2:
        # A. Shchepetkin (UCLA-ROMS, 2005) vertical stretching function.

        alfa = 1.0
        beta = 1.0
        ds = 1.0 / N
        if kgrid == 1:
            Nlev = Np
            lev = np.arange(0, N + 1)
            s = (lev - N) * ds
        else:
            Nlev = N
            lev = np.arange(1, N + 1) - 0.5
            s = (lev - N) * ds

        if theta_s > 0:
            Csur = (1.0 - np.cosh(theta_s * s)) / (np.cosh(theta_s) - 1.0)
            if theta_b > 0:
                Cbot = -1.0 + np.sinh(theta_b * (s + 1.0)) / np.sinh(theta_b)
                power_alpha = np.power((s + 1.0), alfa)
                power_beta = np.power((s + 1.0), beta)
                weigth = power_alpha * (1.0 + (alfa / beta) * (1.0 - power_beta))
                C = weigth * Csur + (1.0 - weigth) * Cbot
            else:
                C = Csur
        else:
            C = s

    elif Vstretching == 3:
        #  R. Geyer BBL vertical stretching function.
        ds = 1.0 / N
        if kgrid == 1:
            nlev = Np
            lev = np.arange(0, N + 1)
            s = (lev - N) * ds
        else:
            Nlev = N
            lev = np.arange(1, N + 1) - 0.5
            s = (lev - N) * ds

        if theta_s > 0:
            exp_s = theta_s  # surface stretching exponent
            exp_b = theta_b  # bottom  stretching exponent
            alpha = 3  # scale factor for all hyperbolic functions
            Cbot = (
                np.log(np.cosh(alpha * np.power((s + 1), exp_b)))
                / np.log(np.cosh(alpha))
                - 1
            )
            Csur = (
                -1
                * np.log(np.cosh(alpha * np.power(abs(s), exp_s)))
                / np.log(np.cosh(alpha))
            )
            weight = (1 - np.tanh(alpha * (s + 0.5))) / 2
            C = weight * Cbot + (1 - weight) * Csur
        else:
            C = s

    elif Vstretching == 4:
        # A. Shchepetkin (UCLA-ROMS, 2010) double vertical stretching function
        # with bottom refinement
        ds = 1.0 / N
        if kgrid == 1:
            Nlev = Np
            lev = np.arange(0, N + 1)
            s = (lev - N) * ds
        else:
            Nlev = N
            lev = np.arange(1, N + 1) - 0.5
            s = (lev - N) * ds

        if theta_s > 0:
            Csur = (1.0 - np.cosh(theta_s * s)) / (np.cosh(theta_s) - 1.0)
        else:
            Csur = np.power(-1 * s, 2)

        if theta_b > 0:
            Cbot = (np.exp(theta_b * Csur) - 1.0) / (1.0 - np.exp(-theta_b))
            C = Cbot
        else:
            C = Csur

    # Report S-coordinate parameters.
    if report:
        Vstretching_info = {
            1: "Song and Haidvogel (1994)",
            2: "Shchepetkin (2005)",
            3: "Geyer (2009), BBL",
            4: "Shchepetkin (2010)",
        }
        print(
            f"Vstretching = {Vstretching}. Reference: {Vstretching_info[Vstretching]}"
        )

        if kgrid == 1:
            print(f"kgrid = {kgrid} at vertical W-points")
        else:
            print(f"kgrid = {kgrid} at vertical RHO-points")

        print(f"theta_s   = {theta_s}")
        print(f"theta_b  = {theta_b}")
        print(f"hc       = {hc}")
        print(f"S-coordinate curves: k, s(k), C(k)")

        for k in range(Nlev, 0, -1):
            info_k = k
            if kgrid == 1:
                info_k = k - 1
            print(f"\t\t{info_k:3g}    {s(k):20.12e}    {C(k):20.12e}")

    return s, C
