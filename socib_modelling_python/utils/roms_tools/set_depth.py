import numpy as np

from .stretching import *


def set_depth(
    Vtransform, Vstretching, theta_s, theta_b, hc, N, igrid, h, zeta=None, report=True
):
    """
    % SET_DEPTH:  Compute ROMS grid depth from vertical stretched variables
    %
    % [z]=set_depth(Vtransform, Vstretching, theta_s, theta_b, hc, N, ...
    %               igrid, h, zeta);
    %
    % Given a batymetry (h), free-surface (zeta) and terrain-following parameters,
    % this function computes the 3D depths for the request C-grid location. If the
    % free-surface is not provided, a zero value is assumef resulting in unperturb
    % depths.  This function can be used when generating initial conditions or
    % climatology data for an application. Check the following link for details:
    %
    %    https://www.myroms.org/wiki/index.php/Vertical_S-coordinate
    %
    % On Input:
    %
    %    Vtransform    Vertical transformation equation:
    %                    Vtransform = 1,   original transformation
    %
    %                      z(x,y,s,t)=Zo(x,y,s)+zeta(x,y,t)*[1+Zo(x,y,s)/h(x,y)]
    %
    %                      Zo(x,y,s)=hc*s+[h(x,y)-hc]*C(s)
    %
    %                    Vtransform = 2,   new transformation
    %
    %                      z(x,y,s,t)=zeta(x,y,t)+[zeta(x,y,t)+h(x,y)]*Zo(x,y,s)
    %
    %                       Zo(x,y,s)=[hc*s(k)+h(x,y)*C(k)]/[hc+h(x,y)]
    %    Vstretching   Vertical stretching function:
    %                    Vstretching = 1,  original (Song and Haidvogel, 1994)
    %                    Vstretching = 2,  A. Shchepetkin (UCLA-ROMS, 2005)
    %                    Vstretching = 3,  R. Geyer BBL refinement
    %                    Vstretching = 4,  A. Shchepetkin (UCLA-ROMS, 2010)
    %    theta_s       S-coordinate surface control parameter (scalar)
    %    theta_b       S-coordinate bottom control parameter (scalar)
    %    hc            Width (m) of surface or bottom boundary layer in which
    %                    higher vertical resolution is required during
    %                    stretching (scalar)
    %    N             Number of vertical levels (scalar)
    %    igrid         Staggered grid C-type (integer):
    %                    igrid=1  => density points
    %                    igrid=2  => streamfunction points
    %                    igrid=3  => u-velocity points
    %                    igrid=4  => v-velocity points
    %                    igrid=5  => w-velocity points
    %    h             Bottom depth, 2D array at RHO-points (m, positive),
    %                    h(1:Lp+1,1:Mp+1).
    %    zeta          Free-surface, 2D array at RHO-points (m), OPTIONAL,
    %                    zeta(1:Lp+1,1:Mp+1).
    %    report        Flag to report detailed information (OPTIONAL):
    %                    report = 0,       do not report
    %                    report = 1,       report information
    %
    % On Output:
    %
    %    z             Depths (m, negative), 3D array.
    %

    % svn $Id: set_depth.m 544 2011-04-01 23:51:17Z arango $
    %==========================================================================%
    %  Copyright (c) 2002-2011 The ROMS/TOMS Group                             %
    %    Licensed under a MIT/X style license                                  %
    %    See License_ROMS.txt                           Hernan G. Arango       %
    %==========================================================================%

    z=[];

    %---------------------------------------------------------------------------
    %  Set several parameters.
    %---------------------------------------------------------------------------
    """

    h = np.array(h)
    zeta = np.array(zeta)
    z = np.zeros(np.append(np.shape(zeta), N))

    # Setting and validating parameters
    if (Vtransform < 1) or (Vtransform > 2):
        print(f"*** Error:  SET_DEPTH - Illegal parameter Vtransform = {Vtransform}")
        return None

    if (Vstretching < 1) or (Vstretching > 4):
        print(f"*** Error:  SET_DEPTH - Illegal parameter Vstretching = {Vstretching}")
        return None

    if (hc > h.min()) and (Vtransform == 1):
        print(
            "*** Error:  SET_DEPTH - critical depth exceeds minimum bathymetry value."
        )
        print(f"                        Vtransform = {Vtransform}")
        print(f"                        hc        = {hc}")
        print(f"                        hmin      = {h.min()}")
        return None

    if zeta is None:
        zeta = np.zeros(np.shape(h))

    Np = N + 1
    Lp, Mp = np.shape(h)
    L = Lp - 1
    M = Mp - 1

    hmin = h.min()
    hmax = h.max()

    if report:
        if Vtransform == 1:
            print(f"Vtransform = {Vtransform} original ROMS")
        elif Vtransform == 2:
            print(f"Vtransform = {Vtransform} original ROMS")
        igrid_info = {
            1: "RHO-points",
            2: "PSI-points",
            3: "U-points",
            4: "V-points",
            5: "RHO-points",
        }
        print(f"\tigrid = {igrid} at horizontal {igrid_info[igrid]}")

    kgrid = 0
    if igrid == 5:
        kgrid = 1

    s, C = stretching(Vstretching, theta_s, theta_b, N, kgrid, report)
    s = np.array(s)
    C = np.array(C)

    # ----------------------------------------------------------------------------
    # Average bathymetry and free-surface at requested C-grid type.
    # ----------------------------------------------------------------------------

    if igrid == 1:
        hr = h
        zetar = zeta
    elif igrid == 2:
        hp = 0.25 * (
            h[0 : L - 1, 0 : M - 1]
            + h[1 : Lp - 1, 0 : M - 1]
            + h[0 : L - 1, 1 : Mp - 1]
            + h[1 : Lp - 1, 1 : Mp - 1]
        )
        zetap = 0.25 * (
            zeta[0 : L - 1, 0 : M - 1]
            + zeta[1 : Lp - 1, 0 : M - 1]
            + zeta[0 : L - 1, 1 : Mp - 1]
            + zeta[1 : Lp - 1, 1 : Mp - 1]
        )
    elif igrid == 3:
        hu = 0.5 * (h[0 : L - 1, 0 : Mp - 1] + h[1 : Lp - 1, 0 : Mp - 1])
        zetau = 0.5 * (zeta[0 : L - 1, 0 : Mp - 1] + zeta[1 : Lp - 1, 0 : Mp - 1])
    elif igrid == 4:
        hv = 0.5 * (h[0 : Lp - 1, 0 : M - 1] + h[0 : Lp - 1, 1 : Mp - 1])
        zetav = 0.5 * (zeta[0 : Lp - 1, 0 : M - 1] + zeta[0 : Lp - 1, 1 : Mp - 1])
    elif igrid == 5:
        hr = h
        zetar = zeta

    # ----------------------------------------------------------------------------
    # Compute depths (m) at requested C-grid location.
    # ----------------------------------------------------------------------------

    if Vtransform == 1:
        indexes = range(N)
        if igrid == 1:
            zeta_var = zetar
            h_var = hr
        elif igrid == 2:
            zeta_var = zetap
            h_var = hp
        elif igrid == 3:
            zeta_var = zetau
            h_var = hu
        elif igrid == 4:
            zeta_var = zetav
            h_var = hv
        elif igrid == 5:
            zeta_var = zetar
            h_var = hr
            z[:, :, 1] = -1 * hr
            indexes = range(1, Np)

        for k in indexes:
            z0 = (s[k] - C[k]) * hc + C[k] * h_var
            z[:, :, k] = z0 + zeta_var * (1.0 + z0 / h_var)

    elif Vtransform == 2:
        indexes = range(N)
        if igrid == 1:
            zeta_var = zetar
            h_var = hr
        elif igrid == 2:
            zeta_var = zetap
            h_var = hp
        elif igrid == 3:
            zeta_var = zetau
            h_var = hu
        elif igrid == 4:
            zeta_var = zetav
            h_var = hv
        elif igrid == 5:
            zeta_var = zetar
            h_var = hr
            z[:, :, 1] = -1 * hr
            indexes = range(Np)

        for k in indexes:
            z0 = (hc * s[k] + C[k] * h_var) / (hc + h_var)
            z[:, :, k] = zeta_var + (zeta_var + h_var) * z0

    return z
