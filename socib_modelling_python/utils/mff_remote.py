# -*- coding: UTF-8 -*-

"""Module with utility functions to assist remote access."""

import re


def valid_url(url):
    """
    Validate if a string is a URL.

    Parameters
    ----------
    url : str
        String to be validated

    Returns
    -------
    is_valid : boolean
       True if is a valid URL or False if it is not.
    """
    try:
        regex = re.compile(
            r"^(?:http|ftp)s?://"  # http:// or https://
            r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|"  # noqa
            r"localhost|"  # localhost...
            r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|"  # ...or ipv4
            r"\[?[A-F0-9]*:[A-F0-9:]+\]?)"  # ...or ipv6
            r"(?::\d+)?"  # optional port
            r"(?:/?|[/?]\S+)$",
            re.IGNORECASE,
        )
        is_valid = re.match(regex, url) is not None
    except Exception:
        is_valid = False
    return is_valid
