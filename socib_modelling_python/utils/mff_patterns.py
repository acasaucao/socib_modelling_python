# -*- coding: UTF-8 -*-

"""This module utility functions to convert/validate patterns."""

from pathlib import Path
import dateutil
import re
import pytz
import json
import tempfile

_TZINFOS = {k: dateutil.tz.gettz(k) for k in pytz.all_timezones}


def parse_date(date, default_tzinfo=dateutil.tz.tzlocal()):
    """Parse a date using dateutil and pytz."""
    parsed_date = dateutil.parser.parse(date, tzinfos=_TZINFOS)
    return parsed_date.replace(tzinfo=parsed_date.tzinfo or default_tzinfo)


def create_dir(pathdir):
    """Check if a directory exist, if not creates it."""
    if not Path(pathdir).exists():
        Path(pathdir).mkdir(parents=True)


def get_tmp_file(only_name=False, **kwargs):
    """Create a temporary file and return the object."""
    temporary_file = tempfile.NamedTemporaryFile(**kwargs)
    if only_name:
        name = temporary_file.name
        temporary_file.close()
        temporary_file = name

    return temporary_file


def is_valid_json(filename):
    """Check if the file is a valid json."""
    try:
        with open(filename, "r") as file_obj:
            json.load(file_obj)
    except Exception:
        return False
    return True


def is_valid_date(date):
    """Check if a string has a valid date."""
    valid = True
    try:
        parse_date(date)
    except Exception:
        # print(F"Error parsing {date}, not valid date")
        valid = False
    return valid


def convert(data, pattern, *arg, **kwargs):
    """
    Convert string with patterns (templates) into path.

    Parameters
    ----------
    data : dict
        Dictionary containing the values that will replace the patterns
    *args
        Not used, for future applications
    **kwargs
        Not used, for future applications

    Returns
    -------
    pattern : str
        Pattern (template) with the values replaced.
    """
    variables = set(re.findall(r"\{([\w|\%:#-]+)\}", pattern))
    for v in variables:
        if ":" in v:
            v_name, v_format = v.split(":")
        else:
            v_name = v
        replacement = data.get(v_name, None)
        if replacement is None:
            print("Remote file pattern is: {}".format(pattern))
            print("Raplacement for '{}' not found!".format(v_name))
            continue
        elif isinstance(replacement, list):
            if "date" not in data:
                print("Remote file pattern is: {}".format(pattern))
                print("{} depends of a date to be selected.".format(v_name))
                print("Options are: {}".format(replacement))
                continue
            else:
                replacement_list = replacement
                date = parse_date(data.get("date", "now"))
                selected_replacement = None
                for opt in replacement_list:
                    start = parse_date(opt[0])
                    end = parse_date(opt[1])
                    if date >= start and date <= end:
                        selected_replacement = opt[2]
                        continue
                if selected_replacement is None:
                    print("Remote file pattern is: {}".format(pattern))
                    print("Raplacement for '{}' not found!".format(v_name))
                    print(
                        "Options are [initial date, final date, option]: {}".format(
                            replacement_list
                        )
                    )
                    continue
            replacement = selected_replacement
        elif v_name == "date":
            base = parse_date(data.get(v_name, "now"))
            replacement = base.strftime(v_format)
        old = "{{{}}}".format(v)
        pattern = pattern.replace(old, replacement)

    return pattern
