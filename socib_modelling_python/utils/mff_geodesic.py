# -*- coding: UTF-8 -*-

"""Script defining utility functions used in geodesic problems."""

from numba import jit
from geopy.distance import geodesic as geopy_geodesic
from geopy.distance import distance as geopy_distance
import numpy as np
from memoization import cached

R_EARTH = 6_378_137  # Earth equatorial radius in meters
R_VOLUMETRIC_EARTH_RADIUS = 6_371_000  # Earth volumetric mean radius in meters
TWO_PI_OVER_360 = 2 * np.pi / 360


def meters_to_degrees(meters, latitude=0, *args, **kwargs):
    """
    Convert a value in meters to a longitude degree in a specific latitude.

    Parameters
    ----------
    meters : float
        The value in meters to be converted
    latitude : float
        latitude location in degrees.
        DEfault value is 0: Equatorial line.
    *args
        Not used, for future applications
    **kwargs
        Not used, for future applications

    Returns
    -------
    degrees for latitude and longitude : float
        The converted value in longitude degrees.
    """
    latitude = np.radians(latitude)
    ratio = meters / (TWO_PI_OVER_360 * R_VOLUMETRIC_EARTH_RADIUS)
    lon_degrees = ratio * (1.0 / np.cos(latitude))
    lat_degrees = ratio

    return lat_degrees, lon_degrees


def distance(point_a, point_b, method="haversine", *args, **kwargs):
    """
    Calculate the distance between to two geographic points.

    Parameters
    ----------
    point_a : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
        Optionally, it can have a third float indicating the vertical position in meters, default 0
    point_b : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
        Optionally, it can have a third float indicating the vertical position in meters, default 0
    method : string
        The method used to calculate de distance.
        Options: 'haversine' (default) and 'geodesic' (vincenty).
    *args
        Not used, for future applications
    **kwargs
        Not used, for future applications

    Returns
    -------
    distance : float
        Distance between point A and point B in meters.
    """
    vertical_a = 0
    vertical_b = 0

    if len(point_a) > 2:
        vertical_a = point_a[2]
        point_a = point_a[:2]

    if len(point_b) > 2:
        vertical_b = point_b[2]
        point_b = point_b[:2]

    delta_vertical = np.float32(vertical_a - vertical_b)
    delta_horizontal = 0

    if method == "haversine":
        delta_horizontal = haversine_distance(point_a, point_b)
    elif method == "geodesic":
        delta_horizontal = geodesic_distance(point_a, point_b)

    distance = np.sqrt(
        delta_horizontal * delta_horizontal + delta_vertical * delta_vertical
    )

    return distance


@cached(max_size=32, ttl=60)
def find_destination(point_a, distance, bearing, vertical_bearing=None):
    """
    Find a destination at a distance from the point A heading at bearing degrees.

    point_a : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
    distance : float
        Distance in meters to the destination point.
    bearing : float
        The bearing setting in which direction is the destination point.
    vertical_bearing : float
        The vertical bearing setting in which direction is the destination point.
        For future implementation of a 3D algorithm.
    Returns
    -------
    destination: list of floats.
       Returns a list of three floats indicating the latitude, longitude, and depth
       of the destination point.
       Currently, as a 3D algorithm is not available, depth is always 0.
    """
    d = geopy_distance(meters=distance)
    destination = d.destination(point=point_a, bearing=bearing)

    return [destination.latitude, destination.longitude, 0]


@cached(max_size=32, ttl=60)
def geodesic_distance(point_a, point_b, *arg, **kwargs):
    """
    Calculate the distance between to two geographic points using Vincenty's method.

    point_a : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
    point_b : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
    n_iter : int
        Number of maximum iterations used to calculated the distance (not implemented)
    """
    distance = geopy_geodesic(point_a, point_b, *arg, **kwargs).meters

    return distance


@jit(nopython=True, cache=True)
def haversine_distance(a, b):
    """
    Calculate the distance between to two points in a sphere using the haversine distance formula.
    This method is adpated from geopy's great_circle method.

    Parameters
    ----------
    point_a : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.
    point_b : tuple of floats
        Tuple of floats with the values of latitude and longitude, respectively.

    Returns
    -------
    distance : float
        Haversine distance between point A and point B.
    """
    lat1, lng1 = np.radians(a[0]), np.radians(a[1])
    lat2, lng2 = np.radians(b[0]), np.radians(b[1])

    sin_lat1, cos_lat1 = np.sin(lat1), np.cos(lat1)
    sin_lat2, cos_lat2 = np.sin(lat2), np.cos(lat2)

    delta_lng = lng2 - lng1
    cos_delta_lng, sin_delta_lng = np.cos(delta_lng), np.sin(delta_lng)

    d = np.arctan2(
        np.sqrt(
            (cos_lat2 * sin_delta_lng) ** 2
            + (cos_lat1 * sin_lat2 - sin_lat1 * cos_lat2 * cos_delta_lng) ** 2
        ),
        sin_lat1 * sin_lat2 + cos_lat1 * cos_lat2 * cos_delta_lng,
    )

    return R_VOLUMETRIC_EARTH_RADIUS * d
