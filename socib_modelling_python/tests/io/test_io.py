# -*- coding: UTF-8 -*-

"""Script to execute unit test of IO module."""

import pytest
import xarray as xr
import shutil
import glob
import socib_modelling_python as mff


def get_all_files_examples():
    """Get local file examples."""
    all_files = [
        ("example.grib", "Gridded binary (GRIB) version 2"),
        ("models/roms_WMOP_sta.nc", "NetCDF Data Format data"),
        ("models/roms_wmop_regular_his_velocities_mallorca.nc.gz", "gzip compressed data")
    ]
    return all_files


def get_all_opendap_examples():
    """Get remote file examples."""
    all_opendap = [
        (
            "http://thredds.socib.es/thredds/dodsC/operational_models/oceanographical/"
            "wave/sapo_mallorca/2019/09/sapo_mallorca_swan_20190904120000.nc"
        )
    ]
    return all_opendap


@pytest.fixture(params=get_all_files_examples())
def example_file(request):
    """Generate local file examples."""
    return request.param


@pytest.fixture(params=get_all_opendap_examples())
def example_opendap(request):
    """Generate remote file examples."""
    return request.param


def test_url_validator(example_opendap):
    """Test the url validator."""
    not_url = "/data/file.nc"
    valid = mff.utils.remote.valid_url(not_url)
    assert valid is False
    valid = mff.utils.remote.valid_url(example_opendap)
    assert valid


class TestInput(object):
    """Testing Class for Input functions."""

    def test_read_opendata(self, example_opendap):
        """Test reading a remote file."""
        data = mff.io.read(example_opendap)
        assert type(data) is xr.Dataset

    def test_read_not_empty_opendata(self, example_opendap):
        """Test if the remote dataset is not empty."""
        data = mff.io.read(example_opendap)
        assert len(data.dims) >= 1

    def test_read_ncdf_file(self, shared_datadir):
        """Test reading a local netcdf file."""
        filename = "{}/models/roms_WMOP_sta.nc".format(shared_datadir)
        ncdf = mff.io.read_ncdf_file(filename)
        assert type(ncdf) is xr.Dataset

    def test_read_not_empty_ncdf_file(self, shared_datadir):
        """Test if the local netcdf file is not empty."""
        filename = "{}/models/roms_WMOP_sta.nc".format(shared_datadir)
        ncdf = mff.io.read_ncdf_file(filename)
        assert len(ncdf.dims) >= 1

    def test_read_gzip_ncdf_file_memory(self, shared_datadir):
        """Test read a gzipped file using only memory."""
        filename = f"{shared_datadir}/models/roms_wmop_regular_his_velocities_mallorca.nc.gz"
        ncdf = mff.io.read(filename)
        assert len(ncdf.dims) >= 1

    def test_read_gzip_ncdf_in_scratch_dir(self, shared_datadir):
        """Test read a gzipped file using only memory."""
        filename = f"{shared_datadir}/models/roms_wmop_regular_his_velocities_mallorca.nc.gz"
        scratch_dir = f"{shared_datadir}/scratch"
        ncdf = mff.io.read(filename, scratch_dir=scratch_dir)
        nfiles = len(glob.glob(f"{scratch_dir}/*"))
        shutil.rmtree(scratch_dir, ignore_errors=False, onerror=None)
        no_files = len(glob.glob(f"{scratch_dir}/*"))
        assert len(ncdf.dims) >= 1
        assert nfiles == 1
        assert no_files == 0

    def test_read_grib_file(self, shared_datadir):
        """Test reading a local grib file."""
        filename = "{}/example.grib".format(shared_datadir)
        grib = mff.io.read_grib_file(filename)
        assert type(grib) is xr.Dataset

    def test_read_not_empty_grib_file(self, shared_datadir):
        """Test if the local grib file is not empty."""
        filename = "{}/example.grib".format(shared_datadir)
        grib = mff.io.read_grib_file(filename)
        assert len(grib.dims) >= 1

    def test_read_file(self, shared_datadir, example_file):
        """Test reading a local file without especify the format."""
        filename = "{}/{}".format(shared_datadir, example_file[0])
        data = mff.io.read_file(filename)
        assert type(data) is xr.Dataset

    def test_read_dir_file(self, shared_datadir):
        """Test reading multiple local files without especify the format."""
        filename = "{}/{}".format(shared_datadir, "multiples")
        data = mff.io.read(filename)
        assert type(data) is xr.Dataset

    def test_read_file_by_pattern(self, shared_datadir):
        """Test reading multiple local files using a pattern wildcard."""
        filename = "{}/{}/{}".format(shared_datadir, "multiples", "roms_WMOP*.nc")
        data = mff.io.read(filename)
        assert type(data) is xr.Dataset

    def test_read_file_with_file_format(self, shared_datadir, example_file):
        """Test reading a local files and checking the format."""
        filename = "{}/{}".format(shared_datadir, example_file[0])
        data = mff.io.read_file(filename)
        assert type(data) is xr.Dataset
        assert example_file[1] in data.file_format

    def test_read_file_with_filename(self, shared_datadir, example_file):
        """Test reading a local files and checking the filename."""
        filename = "{}/{}".format(shared_datadir, example_file[0])
        data = mff.io.read_file(filename)
        assert data.filename == filename

    def test_file_not_found(self, capsys):
        """Test reading a local file that does not exist."""
        filename = "not_found.file"
        data = mff.io.read_file(filename)
        captured = capsys.readouterr()
        msg = "FileNotFoundError: No such file or directory: {}\n".format(filename)
        assert data is None
        assert captured.out == msg

    def test_read_extra_kwargs(self, shared_datadir):
        """Test the propagation of kwargs past to the read function."""
        filename = "{}/{}".format(shared_datadir, "multiples")
        data = mff.io.read(filename, compat="broadcast_equals")
        assert type(data) is xr.Dataset
        assert len(data.dims) >= 1

    def test_read_opendata_extra_kwargs(self, example_opendap):
        """Test the propagation of kwargs past to the read function in a remote context."""
        data = mff.io.read(example_opendap, decode_times=False)
        assert len(data.dims) >= 1
