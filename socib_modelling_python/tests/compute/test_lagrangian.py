# -*- coding: UTF-8 -*-

"""Test Module for lagragian computation."""

import socib_modelling_python as mff
import numpy as np
import pytest


class TestFSLE(object):
    """docstring for TestFSLE."""

    def test_simple_FSLE(self):
        """Testing the FSLE formulation."""
        initial_separation = 2000
        final_separation = 4000
        time = 3600
        correct_fsle = (1.0 / time) * np.log(final_separation / initial_separation)
        calculated_fsle = mff.compute.lagrangian.FSLE(
            initial_separation, final_separation, time
        )

        assert correct_fsle == pytest.approx(calculated_fsle, rel=1e-5)
