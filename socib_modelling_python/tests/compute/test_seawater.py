# -*- coding: UTF-8 -*-

"""Test Module for seawater computation."""

import socib_modelling_python as mff
import numpy as np
import pytest


class TestSeaWater(object):
    """docstring for TestSeaWater."""

    def test_press2depth(self):
        """Testing the press2depth formulation."""
        pressures = np.arange(1000, 10001, 1000)
        pressures = np.insert(pressures, 0, 500)
        latitudes = np.array([0, 30, 45, 60, 90])
        MPRES, MLATS = np.meshgrid(pressures, latitudes)
        correct_depth = np.array(
            [
                [496.65, 496.00, 495.34, 494.69, 494.03],
                [992.12, 990.81, 989.50, 988.19, 986.88],
                [1979.55, 1976.94, 1974.33, 1971.72, 1969.11],
                [2962.43, 2958.52, 2954.61, 2950.71, 2946.81],
                [3940.88, 3935.68, 3930.49, 3925.30, 3920.10],
                [4915.04, 4908.56, 4902.08, 4895.60, 4889.13],
                [5885.03, 5877.27, 5869.51, 5861.76, 5854.01],
                [6850.95, 6841.92, 6832.89, 6823.86, 6814.84],
                [7812.93, 7802.63, 7792.33, 7782.04, 7771.76],
                [8771.07, 8759.51, 8747.95, 8736.40, 8724.85],
                [9725.47, 9712.65, 9699.84, 9687.03, 9674.23],
            ]
        )

        calculated_depth = np.around(
            mff.compute.seawater.press2depth(MPRES.T, MLATS.T), 2
        )

        assert calculated_depth == pytest.approx(correct_depth, rel=1e-1)
