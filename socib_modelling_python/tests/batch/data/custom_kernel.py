# -*- coding: UTF-8 -*-

"""Module defining custom kernels in Parcels format."""


def AgeingTest(particle, fieldset, time):
    """Update particle age."""
    particle.age += particle.dt
