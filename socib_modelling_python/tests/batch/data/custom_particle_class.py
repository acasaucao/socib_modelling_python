# -*- coding: UTF-8 -*-

"""Module defining custom Partciles in Parcels format."""

from parcels import JITParticle, Variable
import numpy as np


class JITAgeingParticleTest(JITParticle):
    """Particle with an age variable."""

    age = Variable("age", dtype=np.float32, initial=0.0)
