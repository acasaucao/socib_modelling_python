# -*- coding: UTF-8 -*-

"""Test Module for tasks with particles."""

import socib_modelling_python as mff
from socib_modelling_python.batch.generate_particles import GenerateParticles
import xarray as xr
import numpy as np
from numpy.testing import assert_array_equal
import pytest
from parcels import JITParticle, ScipyParticle
import json


class TestRunParticles(object):
    """docstring for TestRunParticles."""

    def test_config_dictionary(self, shared_datadir):
        """Testing setting the configuration with a dictionary."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        specs = {
            "kind": "parcels",
            "fieldset": {
                "params": {
                    "field": field,
                    "variables": {
                        "U": "u_velocity",
                        "V": "v_velocity",
                        "W": "w_velocity",
                    },
                    "dimensions": {
                        "time": "time",
                        "depth": "depth",
                        "lat": "latitude",
                        "lon": "longitude",
                    },
                }
            },
            "particles": {
                "generator": {
                    "method": "at_location",
                    "params": {
                        "n_particles": 1,
                        "latitude": [39.6],
                        "longitude": [2.4],
                    },
                },
                "pclass": "JITParticle",
            },
            "kernels": {"sequence": ["AdvectionRK4_3D"]},
            "execution": {
                "dt": {"minutes": 1},
                "outputdt": {"minutes": 5},
                "runtime": {"seconds": 300},
            },
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((39.6, 2.4), rel=1e-5)

    # def test_wmop_particle_simulation_default_config(self, shared_datadir):
    #     """Testing simulate particles over a wmop field."""
    #     n_particles = 100
    #     lats, lons, depths = GenerateParticles.at_location(n_particles,
    #                                                        np.float32(39.2), np.float32(2.3))
    #     particles = [lats, lons, depths]
    #     kernels = ["AdvectionRK4_3D_surface_limited", "periodicBC"]
    #     simulation = mff.batch.run_particles.ParticleSimulation(particles=particles,
    #                                                             kernels=kernels,
    #                                                             boundaries_strategy='periodic')
    #
    #     field = xr.open_dataset(F"{shared_datadir}/roms_wmop_regular_his_velocities_mallorca.nc")
    #     field["w"] = field["w"].astype(np.float32)
    #     field["v"] = field["v"].astype(np.float32)
    #     field["u"] = field["u"].astype(np.float32)
    #
    #     simulation.set_field(field)
    #
    #     simulation.build()
    #     simulation.run()
    #
    #     rng = np.random.default_rng(1)
    #     pid = rng.integers(n_particles, size=2)
    #
    #     p1 = simulation.get_particle(pid[0])
    #     position1 = (p1.latitude, p1.longitude, p1.depth)
    #     p2 = simulation.get_particle(pid[1])
    #     position2 = (p2.latitude, p2.longitude, p2.depth)
    #
    #     assert position1 == position2

    def test_static_particle(self, shared_datadir):
        """Testing one particle in a field with velocities equal 0."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((39.6, 2.4), rel=1e-5)

    def test_only_u_velocity_particle(self, shared_datadir):
        """Testing one particle with contant velocities equal 1 only in the u-component."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])
        initial_location = (39.6, 2.4)
        distance = mff.utils.geodesic.distance(
            initial_location, position, method="geodesic"
        )
        correct_distance = 300  # 300 meters = velocite (1 m/s) * time (300s)

        assert particle["longitude"] == pytest.approx(initial_location[1], rel=1e-2)
        assert correct_distance == pytest.approx(distance, rel=1e-1)

    def test_only_v_velocity_particle(self, shared_datadir):
        """Testing one particle with contant velocities equal 1 only in the v-component."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])
        initial_location = (39.6, 2.4)
        distance = mff.utils.geodesic.distance(
            initial_location, position, method="geodesic"
        )
        correct_distance = 300  # 300 meters = velocite (1 m/s) * time (300s)

        assert particle["latitude"] == pytest.approx(initial_location[0], rel=1e-2)
        assert correct_distance == pytest.approx(distance, rel=1e-1)

    def test_only_w_velocity_particle(self, shared_datadir):
        """Testing one particle with contant velocities equal 1 only in the w-component."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32)
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"], particle["depth"])
        initial_location = (39.6, 2.4)
        distance = mff.utils.geodesic.distance(
            initial_location, position, method="geodesic"
        )
        correct_distance = 300  # 300 meters = velocite (1 m/s) * time (300s)

        assert position[0:2] == pytest.approx(initial_location, rel=1e-2)
        assert correct_distance == pytest.approx(distance, rel=1e-1)

    def test_particle(self, shared_datadir):
        """Testing one particle with contant velocities equal 1."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"], particle["depth"])
        initial_location = (39.6, 2.4)
        distance = mff.utils.geodesic.distance(
            initial_location, position, method="geodesic"
        )
        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance, rel=1e-1)

    def test_particle_generator_at_location(self, shared_datadir):
        """Testing generation particles at a specific location."""
        n_particles = 100
        initial_location = (39.6, 2.4)
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"]["params"] = {
            "n_particles": n_particles,
            "latitude": initial_location[0],
            "longitude": initial_location[1],
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)
        assert position1 == position2

    def test_particle_generator_at_location_with_dispersion(self, shared_datadir):
        """Testing generation particles at a specific location with a dispersion factor."""
        n_particles = 100
        initial_location = (39.6, 2.4)
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"]["params"] = {
            "n_particles": n_particles,
            "latitude": initial_location[0],
            "longitude": initial_location[1],
            "dispersion": 100,
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)
        assert position1 != position2

    def test_particle_generator_regular(self, shared_datadir):
        """Testing generation particles with a regular space between them."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        separation = 1000
        lat_limits = (field["latitude"].min(), field["latitude"].max())
        lon_limits = (field["longitude"].min(), field["longitude"].max())

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"] = {
            "method": "regular",
            "params": {
                "lat_limits": lat_limits,
                "lon_limits": lon_limits,
                "lat_separation": separation,
                "lon_separation": separation,
            },
        }
        specs["strategies"] = {"boundaries": "periodic"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        n_particles = len(particles[0])

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)
        assert position1 != position2

    def test_particle_generator_random(self, shared_datadir):
        """Testing generation particles at a random location."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        n_particles = 100
        lat_limits = (field["latitude"].min(), field["latitude"].max())
        lon_limits = (field["longitude"].min(), field["longitude"].max())

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"] = {
            "method": "random",
            "params": {
                "n_particles": n_particles,
                "lat_limits": lat_limits,
                "lon_limits": lon_limits,
            },
        }
        specs["strategies"] = {"boundaries": "periodic"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)

    def test_particle_generator_from_grid(self, shared_datadir):
        """Testing generation particles at all grid points."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"] = {
            "method": "from_grid",
            "params": {"grid": field},
        }
        specs["strategies"] = {"boundaries": "periodic"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        n_particles = len(particles[0])

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)

    def test_particle_generator_from_grid_with_land(self, shared_datadir):
        """Testing generation particles at all grid points excluding land points."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        da = field["u_velocity"].isel(time=0, depth=0)
        land = np.isnan(da)
        field["land_mask"] = land
        land_coords = {
            "latitude": "latitude",
            "longitude": "longitude"
        }

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["generator"] = {
            "method": "from_grid",
            "params": {
                "grid": field,
                "land_mask": field["land_mask"],
                "land_mask_coords": land_coords,
            },
        }
        specs["strategies"] = {"boundaries": "periodic"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        n_particles = len(particles[0])

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)

    def test_wmop_particle_simulation_at_location(self, shared_datadir):
        """Testing simulate particles over a wmop field."""
        n_particles = 100
        initial_location = (39.2, 2.3)

        field = xr.open_dataset(
            f"{shared_datadir}/roms_wmop_regular_his_velocities_mallorca.nc"
        )
        field["w"] = field["w"].astype(np.float32)
        field["v"] = field["v"].astype(np.float32)
        field["u"] = field["u"].astype(np.float32)

        with open(
            f"{shared_datadir}/wmop_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4"]
        specs["particles"]["generator"]["params"] = {
            "n_particles": n_particles,
            "latitude": initial_location[0],
            "longitude": initial_location[1],
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])

        assert position1 == position2

    def test_wmop_particle_simulation_near_coast(self, shared_datadir):
        """Testing simulate particles over a wmop field near coast."""
        n_particles = 100
        initial_location = (39.5, 2.72)

        field = xr.open_dataset(
            f"{shared_datadir}/roms_wmop_regular_his_velocities_mallorca.nc"
        )
        field["w"] = field["w"].astype(np.float32)
        field["v"] = field["v"].astype(np.float32)
        field["u"] = field["u"].astype(np.float32)

        with open(
            f"{shared_datadir}/wmop_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4"]
        specs["particles"]["generator"]["params"] = {
            "n_particles": n_particles,
            "latitude": initial_location[0],
            "longitude": initial_location[1],
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])

        assert position1 == position2

    def test_wmop_particle_simulation_from_grid_with_land(self, shared_datadir):
        """Testing simulate particles over a wmop field with particles in all grid points."""
        field = xr.open_dataset(
            f"{shared_datadir}/roms_wmop_regular_his_velocities_mallorca.nc"
        )
        field["w"] = field["w"].astype(np.float32)
        field["v"] = field["v"].astype(np.float32)
        field["u"] = field["u"].astype(np.float32)

        da = field["w"].isel(ocean_time=0)
        land = np.isnan(da)
        field["land_mask"] = land
        land_coords = {
            "latitude": "lat",
            "longitude": "lon",
            "depth": "depth"
        }

        with open(
            f"{shared_datadir}/wmop_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)

        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4", "periodicBC"]
        specs["particles"]["generator"] = {
            "method": "from_grid",
            "params": {
                "grid": field,
                "latitude": "lat",
                "longitude": "lon",
                "land_mask": field["land_mask"],
                "land_mask_coords": land_coords
            },
        }
        specs["strategies"] = {"boundaries": "periodic"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()

    def test_particle_boundary_deletion(self, shared_datadir):
        """Testing simulate particles deleting the particles that cross the boundaries."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32)
        field["v_velocity"] = field["v_velocity"].astype(np.float32)
        field["u_velocity"] = field["u_velocity"].astype(np.float32)

        n_particles = 100
        lat_limits = (field["latitude"].min(), field["latitude"].max())
        lon_limits = (field["longitude"].min(), field["longitude"].max())

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4"]
        specs["particles"]["generator"] = {
            "method": "random",
            "params": {
                "n_particles": n_particles,
                "lat_limits": lat_limits,
                "lon_limits": lon_limits,
            },
        }
        specs["strategies"] = {"boundaries": "DeleteParticle"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        position1 = (p1["latitude"], p1["longitude"], p1["depth"])
        distance1 = mff.utils.geodesic.distance(
            (particles[0][pid[0]], particles[1][pid[0]]), position1
        )

        p2 = simulation.get_particle_location(pid[1])
        position2 = (p2["latitude"], p2["longitude"], p2["depth"])
        distance2 = mff.utils.geodesic.distance(
            (particles[0][pid[1]], particles[1][pid[1]]), position2
        )

        correct_distance = np.sqrt(2) * 300  # resultant speed of sqrt(2) m/s * 300s

        assert correct_distance == pytest.approx(distance1, rel=1e-1)
        assert correct_distance == pytest.approx(distance2, rel=1e-1)

    def test_kernel_zero_depth_limted(self, shared_datadir):
        """Testing generation particles at a random location."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * -1
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        n_particles = 100
        lat_limits = (field["latitude"].min(), field["latitude"].max())
        lon_limits = (field["longitude"].min(), field["longitude"].max())

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4_3D_surface_limited"]
        specs["particles"]["generator"] = {
            "method": "random",
            "params": {
                "n_particles": n_particles,
                "lat_limits": lat_limits,
                "lon_limits": lon_limits,
                "depth_limits": (0, 100),
            },
        }
        specs["strategies"] = {"boundaries": "DeleteParticle"}

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()

        rng = np.random.default_rng(1)
        pid = rng.integers(n_particles, size=2)

        p1 = simulation.get_particle_location(pid[0])
        p2 = simulation.get_particle_location(pid[1])

        assert p1["depth"] == 0
        assert p2["depth"] == 0

    def test_associated_4NN2D(self):
        """Testing the 4NN2D strategy."""
        latitudes = [30, 30, 30, 30.5, 30.5, 30.5, 31, 31, 31]
        longitudes = [0, 0.5, 1, 0, 0.5, 1, 0, 0.5, 1]
        associations = GenerateParticles.associated_4NN2D(latitudes, longitudes, (3, 3))
        p_middle = 4
        p_corner = 0
        p_corner_top = 8
        p_side = 5

        assert_array_equal(associations[p_middle], [7, 5, 3, 1])
        assert_array_equal(associations[p_corner], [3, 1, -1, -1])
        assert_array_equal(associations[p_corner_top], [7, 5, -1, -1])
        assert_array_equal(associations[p_side], [8, 4, 2, -1])

    def test_associated_8NN2D(self):
        """Testing the 8NN2D strategy."""
        latitudes = [30, 30, 30, 30.5, 30.5, 30.5, 31, 31, 31]
        longitudes = [0, 0.5, 1, 0, 0.5, 1, 0, 0.5, 1]
        associations = GenerateParticles.associated_8NN2D(latitudes, longitudes, (3, 3))
        p_middle = 4
        p_corner = 0
        p_corner_top = 8
        p_side = 5

        assert_array_equal(associations[p_middle], [8, 7, 6, 5, 3, 2, 1, 0])
        assert_array_equal(associations[p_corner], [4, 3, 1, -1, -1, -1, -1, -1])
        assert_array_equal(associations[p_corner_top], [7, 5, 4, -1, -1, -1, -1, -1])
        assert_array_equal(associations[p_side], [8, 7, 4, 2, 1, -1, -1, -1])

    def test_partcile_factory_parcels(self):
        """Testing the particle factory."""
        factory = mff.batch.factories.particles_factories.ParticleAbstractFactory()
        scipyparticle = factory.produce("ScipyParticle")
        jitParticle = factory.produce("JITParticle")
        jitParticleAssociated = factory.produce("JITParticleAssociated")

        assert issubclass(scipyparticle, ScipyParticle)
        assert issubclass(jitParticle, JITParticle)
        assert issubclass(
            jitParticleAssociated,
            mff.batch.factories.custom_particles_parcels.JITParticleAssociated,
        )

    def test_static_JITParticleAssociated(self, shared_datadir):
        """Testing one particle in a field with velocities equal 0."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["particles"]["pclass"] = "JITParticleAssociated"

        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((39.6, 2.4), rel=1e-5)

    def test_simulation_factory(self, shared_datadir):
        """Testing run a simulation using the Factory Pattern."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        specs = {
            "kind": "parcels",
            "fieldset": {
                "params": {
                    "field": field,
                    "variables": {
                        "U": "u_velocity",
                        "V": "v_velocity",
                        "W": "w_velocity",
                    },
                    "dimensions": {
                        "time": "time",
                        "depth": "depth",
                        "lat": "latitude",
                        "lon": "longitude",
                    },
                }
            },
            "particles": {
                "generator": {
                    "method": "at_location",
                    "params": {
                        "n_particles": 1,
                        "latitude": [39.6],
                        "longitude": [2.4],
                    },
                },
                "pclass": "JITParticle",
            },
            "kernels": {"sequence": ["AdvectionRK4_3D"]},
            "execution": {
                "dt": {"minutes": 1},
                "outputdt": {"minutes": 5},
                "runtime": {"seconds": 300},
            },
        }
        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((39.6, 2.4), rel=1e-5)

    def test_JITParticleAssociated_association(self, shared_datadir):
        """Testing setting the configuration with a dictionary."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        da = field["u_velocity"].isel(time=0, depth=0)
        land = np.isnan(da)
        field["land_mask"] = land
        land_coords = {
            "latitude": "latitude",
            "longitude": "longitude"
        }

        with open(
            f"{shared_datadir}/toy_unit_velocities_config_ocean_parcels.json", "r"
        ) as cfg:
            specs = json.load(cfg)
        specs["fieldset"]["params"]["field"] = field
        specs["kernels"]["sequence"] = ["AdvectionRK4_3D_surface_limited"]
        specs["particles"]["pclass"] = "JITParticleAssociated"
        specs["particles"]["pclass_config"] = {"n_associations": 4, "associated": True}
        specs["particles"]["generator"] = {
            "method": "associated_particles",
            "params": {
                "grid": field,
                "latitude": "latitude",
                "longitude": "longitude",
                "land_mask": field["land_mask"],
                "land_mask_coords": land_coords,
                "strategy": "4NN2D",
            },
        }

        simulation = mff.batch.SimulationFactory.produce(specs)
        particles = simulation.get_all_particles_location()
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((particles[0][0], particles[1][0]), rel=1e-5)

    def test_custom_kernels_and_particles(self, shared_datadir):
        """Testing run a simulation using the Factory Pattern."""
        field = xr.open_dataset(f"{shared_datadir}/toy_unit_velocities.nc")
        field["w_velocity"] = field["w_velocity"].astype(np.float32) * 0
        field["v_velocity"] = field["v_velocity"].astype(np.float32) * 0
        field["u_velocity"] = field["u_velocity"].astype(np.float32) * 0

        custom_kernels = f"{shared_datadir}/custom_kernel.py"
        custom_particles = f"{shared_datadir}/custom_particle_class.py"

        specs = {
            "kind": "parcels",
            "fieldset": {
                "params": {
                    "field": field,
                    "variables": {
                        "U": "u_velocity",
                        "V": "v_velocity",
                        "W": "w_velocity",
                    },
                    "dimensions": {
                        "time": "time",
                        "depth": "depth",
                        "lat": "latitude",
                        "lon": "longitude",
                    },
                }
            },
            "particles": {
                "generator": {
                    "method": "at_location",
                    "params": {
                        "n_particles": 1,
                        "latitude": [39.6],
                        "longitude": [2.4],
                    },
                },
                "custom_particles_path": custom_particles,
                "pclass": "JITAgeingParticleTest",
            },
            "kernels": {
                "sequence": ["AgeingTest", "AdvectionRK4_3D"],
                "custom_kernels_path": custom_kernels
            },
            "execution": {
                "dt": {"minutes": 1},
                "outputdt": {"minutes": 5},
                "runtime": {"seconds": 300},
            },
        }
        simulation = mff.batch.SimulationFactory.produce(specs)
        simulation.run()
        particle = simulation.get_particle_location(0)
        position = (particle["latitude"], particle["longitude"])

        assert position == pytest.approx((39.6, 2.4), rel=1e-5)
        assert int(round(simulation.get_particle(0).age)) == 300
