# -*- coding: UTF-8 -*-

"""Test batch Module that loads a model scenario (forecast, reanalysis, etc)."""

import socib_modelling_python as mff
import numpy as np


class TestLoadWMOPForecast(object):
    """Test load a WMOP forecast scenario."""

    def test_load_latest_day_forecast(self):
        """Testing loading one day forecast."""
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="latest"
        )
        n_forecast = wmop.ocean_time.size

        assert n_forecast == 8

    def test_load_one_day_forecast(self):
        """Testing loading one day forecast."""
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="20210125"
        )
        n_forecast = wmop.ocean_time.size
        last_time = wmop.ocean_time.values.max()

        assert n_forecast == 8
        assert last_time == np.datetime64("2021-01-25T21:00:00.000000000")

    def test_load_forecast(self):
        """Testing loading n days forecast."""
        period = ("20210123", "20210125")
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period=period
        )
        n_forecast = wmop.ocean_time.size
        last_time = wmop.ocean_time.values.max()

        assert n_forecast == 24
        assert last_time == np.datetime64("2021-01-25T21:00:00.000000000")

    def test_load_forecast_subregion(self):
        """Testing loading alone a specific subregion forecast."""
        region = "BalearicIslands"
        limits = (-1, 5.5, 37.8, 41.5)
        period = ("20210123", "20210125")
        sub_region = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period=period, region=region
        )

        sub_lon_min = sub_region["lon_rho"].values.min()
        sub_lon_max = sub_region["lon_rho"].values.max()
        sub_lat_min = sub_region["lat_rho"].values.min()
        sub_lat_max = sub_region["lat_rho"].values.max()

        assert sub_lon_min >= limits[0]
        assert sub_lon_max <= limits[1]
        assert sub_lat_min >= limits[2]
        assert sub_lat_max <= limits[3]

    def test_load_one_variable(self):
        """Testing loading one variable."""
        variables = ["u"]
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="20210125", variables=variables
        )

        assert variables == list(wmop.data_vars)

    def test_load_multiple_variable(self):
        """Testing loading multiples variables."""
        variables = ["u", "v"]
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="20210125", variables=variables
        )

        assert variables == list(wmop.data_vars)

    def test_load_one_variable_from_translation(self):
        """Testing loading variable from a common name, requires translation."""
        variables = ["salt"]
        trans_variables = ["salinity"]
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="20210125", variables=trans_variables
        )

        assert variables == list(wmop.data_vars)

    def test_load_one_variable_from_original_name(self):
        """Testing loading variable from the original name, does not requires translation."""
        variables = ["zeta"]
        wmop = mff.batch.oceanic_models.load_oceanic_model(
            model="wmop_surface", period="20210125", variables=variables
        )

        assert variables == list(wmop.data_vars)
