# -*- coding: UTF-8 -*-

"""Script to execute unit test of grid transformation module."""

import socib_modelling_python as mff


class Test_Transform_Grid(object):
    """Test set of grid transformation module."""

    def test_crop_grid_tuple(self):
        """Test Crop a grid using tuple of floats."""
        model = mff.miner.latest_wmop_3d()
        limits = (-5.8, 0, 34.9, 37.8)
        sub_region = mff.transform.grid.crop_grid(model, limits)

        sub_lon_min = sub_region["lon_rho"].values.min()
        sub_lon_max = sub_region["lon_rho"].values.max()
        sub_lat_min = sub_region["lat_rho"].values.min()
        sub_lat_max = sub_region["lat_rho"].values.max()

        assert sub_lon_min >= limits[0]
        assert sub_lon_max <= limits[1]
        assert sub_lat_min >= limits[2]
        assert sub_lat_max <= limits[3]

    def test_crop_grid_region(self):
        """Test Crop a grid using a name region."""
        model = mff.miner.latest_wmop_3d()
        region = "BalearicIslands"
        limits = (-1, 5.5, 37.8, 41.5)
        sub_region = mff.transform.grid.crop_grid(model, region)

        sub_lon_min = sub_region["lon_rho"].values.min()
        sub_lon_max = sub_region["lon_rho"].values.max()
        sub_lat_min = sub_region["lat_rho"].values.min()
        sub_lat_max = sub_region["lat_rho"].values.max()

        assert sub_lon_min >= limits[0]
        assert sub_lon_max <= limits[1]
        assert sub_lat_min >= limits[2]
        assert sub_lat_max <= limits[3]

    def test_crop_grid_with_custom_coords(self):
        """Test Crop a grid using custom coordinates name."""
        model = mff.miner.latest_wmop_3d()
        region = "BalearicIslands"
        limits = (-1, 5.5, 37.8, 41.5)
        coords = {"x": "lon_uv", "y": "lat_uv"}
        sub_region = mff.transform.grid.crop_grid(model, region, coord_dict=coords)

        sub_lon_min = sub_region[coords["x"]].values.min()
        sub_lon_max = sub_region[coords["x"]].values.max()
        sub_lat_min = sub_region[coords["y"]].values.min()
        sub_lat_max = sub_region[coords["y"]].values.max()

        assert sub_lon_min >= limits[0]
        assert sub_lon_max <= limits[1]
        assert sub_lat_min >= limits[2]
        assert sub_lat_max <= limits[3]

    def test_transform_s_grid_to_regular(self):
        """Test transform a S-Grid into a Regular Grid."""
        a = 1

        assert 1 == a
