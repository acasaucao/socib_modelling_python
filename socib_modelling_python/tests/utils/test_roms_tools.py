import pytest
import numpy as np
import os
import json

import socib_modelling_python.utils as mff_utils

MY_PATH = os.path.dirname(__file__)


def get_examples_stretching():
    file_examples = "{}/data/roms_tools/examples.json".format(MY_PATH)
    with open(file_examples, "r") as f:
        all_examples = json.load(f).get("stretching")
    return all_examples


@pytest.fixture(params=get_examples_stretching())
def example_stretching(request):
    return request.param


def get_examples_set_depth():
    file_examples = "{}/data/roms_tools/examples.json".format(MY_PATH)
    with open(file_examples, "r") as f:
        all_examples = json.load(f).get("set_depth")
    return all_examples


@pytest.fixture(params=get_examples_set_depth())
def example_set_depth(request):
    return request.param


@pytest.mark.skip(reason="This module is too experimental for CI/CD")
class TestRomsTools(object):
    """docstring for TestRomsTools."""

    def test_stretching(self, example_stretching):
        params = example_stretching.get("params", {})
        result = example_stretching.get("result", {})
        s, C = mff_utils.roms_tools.stretching(**params)
        assert s == pytest.approx(result["s"], abs=0.0001)
        assert C == pytest.approx(result["C"], abs=0.0001)

    def test_set_depth(self, example_set_depth):
        params = example_set_depth.get("params", {})
        result = example_set_depth.get("result", {})
        z = mff_utils.roms_tools.set_depth(**params)
        reference = result["z"]
        reference = np.moveaxis(reference, 0, -1)
        print(np.shape(z))
        print(z)
        print(np.shape(reference))
        print(reference)
        assert z == pytest.approx(reference, abs=0.0001)
