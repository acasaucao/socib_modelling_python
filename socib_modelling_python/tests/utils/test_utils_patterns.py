# -*- coding: UTF-8 -*-

"""Script to execute unit test of mff_patterns module."""

import pytest
import socib_modelling_python as mff


def get_example_pattern_dates():
    """
    List of patterns example.

    List of date patterns replacement
    """
    examples = [
        ({"date": "20190901"}, "file_path/{date:%Y}", "file_path/2019"),
        ({"date": "20090120"}, "file_path/{date:%m}", "file_path/01"),
        (
            {"date": "19960512"},
            "file_path/{date:%Y}/{date:%m}/filename_{date:%Y%m%d}",
            "file_path/1996/05/filename_19960512",
        ),
        (
            {
                "date": "20090120",
                "id": [
                    ["20190901", "20190910", "id10"],
                    ["20090101", "20090930", "id4"],
                ],
            },
            "file_path/{id}",
            "file_path/id4",
        ),
    ]
    return examples


@pytest.fixture(params=get_example_pattern_dates())
def example_pattern_dates(request):
    """
    Generate of patterns example.

    Generates a pattern example based on the example list.
    """
    return request.param


class Test_Utils_Patterns(object):
    """Test set for patterns module from utility dir."""

    def test_conversion_date(self, example_pattern_dates):
        """
        Test date pattern conversion.

        Test the conversion of a date pattern into a string date.
        """
        converted = mff.utils.patterns.convert(
            example_pattern_dates[0], example_pattern_dates[1]
        )
        assert converted == example_pattern_dates[2]
