# -*- coding: UTF-8 -*-

"""Script to execute unit test of mff_patterns module."""

import pytest
import socib_modelling_python as mff


class Test_Utils_Geodesic(object):
    """Test set for patterns module from utility dir."""

    def test_haversine_distance_1(self):
        """
        Test haversine distance function.

        Test the distance between two points using the haversine function.
        """
        point_a = (50, 50)
        point_b = (60, 60)
        correct_distance = 1278732.13986491
        distance = mff.utils.geodesic.haversine_distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)

    def test_haversine_distance_2(self):
        """
        Test haversine distance function.

        Test the distance between two points using the haversine function.
        """
        point_a = (60, 2.5)
        point_b = (60, 2.51)
        correct_distance = 555.9754180917
        distance = mff.utils.geodesic.haversine_distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)

    def test_distance_1(self):
        """
        Test distance function with haversine as default.

        Test the distance between two points using two coordinates and the haversine function.
        """
        point_a = (60, 2.5)
        point_b = (60, 2.51)
        correct_distance = 555.9754180917
        distance = mff.utils.geodesic.distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)

    def test_distance_2(self):
        """
        Test distance function with haversine as default.

        Test the distance between two points using 3 coordinates.
        """
        point_a = (60, 2.5, 0)
        point_b = (60, 2.51, 0)
        correct_distance = 555.9754180917
        distance = mff.utils.geodesic.distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)

    def test_distance_3(self):
        """
        Test distance function with haversine as default.

        Test the distance between two points using a 2 and a 3 coordinates point.
        """
        point_a = (60, 2.5)
        point_b = (60, 2.51, 0)
        correct_distance = 555.9754180917
        distance = mff.utils.geodesic.distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)

    def test_distance_4(self):
        """
        Test distance function with haversine as default.

        Test the distance between two points with vertical displacement.
        """
        point_a = (60, 2.5)
        point_b = (60, 2.5, 100)
        correct_distance = 100
        distance = mff.utils.geodesic.distance(point_a, point_b)

        assert correct_distance == pytest.approx(distance, rel=1e-5)
