# -*- coding: UTF-8 -*-

"""Script to execute unit test of miner module."""

import pytest
import xarray as xr
import socib_modelling_python as mff
import json
import os

_MFF_CONFIG = mff.config.Config()

MY_PATH = os.path.dirname(__file__)


def get_socib_example():
    """Create some examples."""
    file_examples = "{}/data/socib_examples.json".format(MY_PATH)
    with open(file_examples, "r") as f:
        all_examples = json.load(f).get("examples")
    return all_examples


@pytest.fixture(params=get_socib_example())
def example_socib_file(request):
    """Generate some examples."""
    return request.param


@pytest.mark.skip(reason="It is a too exaustive test fo CI/CD")
class TestMinerSocib(object):
    """docstring for TestMinerSocibModelsWMOP."""

    def test_get_file_location(self, example_socib_file):
        """Test if remote location is correct."""
        miner = mff.miner.Miner()
        select = example_socib_file.get("select", None)
        crawler_kwargs = dict(select=select)

        location = miner.crawl_remote_location(
            product_name=example_socib_file["product"],
            date=example_socib_file["date"],
            crawler_kwargs=crawler_kwargs,
        )
        assert example_socib_file["url"] in location

    def test_load_full_fields(self, example_socib_file):
        """Test if the mined product has the correct fields."""
        miner = mff.miner.Miner()
        select = example_socib_file.get("select", None)
        include = example_socib_file.get("include", None)
        data = miner.mine_product(
            product_name=example_socib_file["product"],
            date=example_socib_file["date"],
            select=select,
            include=include,
        )

        assert type(data) is xr.Dataset
        assert len(data.dims) == example_socib_file["dims"]
        if "checks" in example_socib_file:
            for key, value in example_socib_file["checks"]:
                assert data.attrs[key] == value

    def test_get_info_product(self, example_socib_file):
        """Test if the miner can get the remote information."""
        miner = mff.miner.Miner()
        info = miner.info_product(example_socib_file["product"])
        assert type(info) is dict

    def test_list_products(self, example_socib_file):
        """Test the miner can list the available products."""
        miner = mff.miner.Miner()
        reference = _MFF_CONFIG.get("miner")
        products = miner.list_products()
        assert products == reference.get("products").keys()
