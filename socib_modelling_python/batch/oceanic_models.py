# -*- coding: UTF-8 -*-

"""Module with methods to process oceanic models like WMOP, MFS and IBI."""

import pendulum
import xarray as xr
from tqdm import tqdm

from ..io import MFFLog
from ..io import read as socib_read
from .. import utils as mff_utils
from .. import transform as mff_transform
from ..config import Config as MFF_Config


_MFF_CONFIG = MFF_Config()
_config = _MFF_CONFIG.get("miner")


def load_oceanic_model(model="wmop", period="latest", *args, **kwargs):
    """Load a model scenario."""
    _logger = MFFLog().getLogger(__name__)

    if model == "WMOP_FORECAST_reg_avg":
        return load_model_wmop_regular_avg_local(period, *args, **kwargs)
    elif model == "WMOP_FORECAST_surface_his":
        return load_model_wmop_surface_his_local(period, *args, **kwargs)
    elif model == "wmop_3d":
        return load_model_wmop_regular_avg_remote(period, *args, **kwargs)
    elif model == "wmop_surface":
        return load_model_wmop_surface_his_remote(period, *args, **kwargs)
    elif model == "ibi_daily_analysis":
        return load_model_ibi_daily_analysis_local(period, *args, **kwargs)
    else:
        _logger.error(f"{model} is not a valid oceanic model!")


def load_model_wmop_regular_avg_local(period="latest", *args, **kwargs):
    """Load WMOP regular average files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 3)
    model_time_span = kwargs.pop("model_time_span", 72)

    product_info = _config.get("products").get("wmop_3d")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    if variables:
        model_params = product_info.get("characteristics").get("parameters")
        model_variables = [model_params.get(v, v) for v in variables]
        variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        model_params = product_info.get("characteristics").get("parameters")
        kwargs["rename_vars"] = {}
        for k, v in model_params.items():
            if isinstance(v, str):
                kwargs["rename_vars"][v] = k

    return load_model_wmop(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        local_info,
        template_filename,
        variables=variables,
        **kwargs
    )


def load_model_wmop_surface_his_local(period="latest", *args, **kwargs):
    """Load WMOP surface his files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 3)
    model_time_span = kwargs.pop("model_time_span", 72)

    product_info = _config.get("products").get("wmop_surface")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    if variables:
        model_params = product_info.get("characteristics").get("parameters")
        model_variables = [model_params.get(v, v) for v in variables]
        variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        model_params = product_info.get("characteristics").get("parameters")
        kwargs["rename_vars"] = {}
        for k, v in model_params.items():
            if isinstance(v, str):
                kwargs["rename_vars"][v] = k

    return load_model_wmop(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        local_info,
        template_filename,
        variables=variables,
        **kwargs
    )


def load_model_wmop_regular_avg_remote(period="latest", *args, **kwargs):
    """Load WMOP surface his files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 3)
    model_time_span = kwargs.pop("model_time_span", 72)

    product_info = _config.get("products").get("wmop_3d")
    thredds_info = product_info["repository"].get("thredds", {})
    template_filename = thredds_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    if variables:
        model_params = product_info.get("characteristics").get("parameters")
        model_variables = [model_params.get(v, v) for v in variables]
        variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        model_params = product_info.get("characteristics").get("parameters")
        kwargs["rename_vars"] = {}
        for k, v in model_params.items():
            if isinstance(v, str):
                kwargs["rename_vars"][v] = k

    return load_model_wmop(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        thredds_info,
        template_filename,
        variables=variables,
        **kwargs
    )


def load_model_wmop_surface_his_remote(period="latest", *args, **kwargs):
    """Load WMOP surface his files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 3)
    model_time_span = kwargs.pop("model_time_span", 72)

    product_info = _config.get("products").get("wmop_surface")
    thredds_info = product_info["repository"].get("thredds", {})
    template_filename = thredds_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    if variables:
        model_params = product_info.get("characteristics").get("parameters")
        model_variables = [model_params.get(v, v) for v in variables]
        variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        model_params = product_info.get("characteristics").get("parameters")
        kwargs["rename_vars"] = {}
        for k, v in model_params.items():
            if isinstance(v, str):
                kwargs["rename_vars"][v] = k

    return load_model_wmop(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        thredds_info,
        template_filename,
        variables=variables,
        **kwargs
    )


def load_model_wmop(
    period,
    region,
    time_window,
    model_dt,
    model_time_span,
    product_info,
    template_filename,
    variables=None,
    **kwargs
):
    """Load WMOP model."""
    _logger = MFFLog().getLogger(__name__)

    kwargs.pop("scratch_dir", None)
    rename_vars = kwargs.pop("rename_vars", None)

    if region:
        _logger.info(f"Loading for subregion: {region}")

    max_failed = model_time_span // time_window
    failed = 0

    dataset = None

    if isinstance(period, str):
        period = (period, period)

    if isinstance(period, tuple):
        if period[1] == "latest":
            now = pendulum.now()
            period = (now, now)
        if isinstance(period[0], str):
            start = mff_utils.patterns.parse_date(period[0])
        else:
            start = period[0]
        if isinstance(period[1], str):
            end = mff_utils.patterns.parse_date(period[1])
        else:
            end = period[1]

        period = pendulum.period(start, end)

        for date in tqdm(period.range("days")):
            horizon = (
                time_window + (min(failed, max_failed) * time_window)
            ) // model_dt
            if dataset is None:
                horizon = time_window // model_dt
            try:
                sample_file = template_filename.format(date=date, **product_info)
                _logger.info(f"Loading {sample_file} +{horizon*model_dt}h")

                sample = socib_read(sample_file, **kwargs)

                if sample is None:
                    raise ValueError

                if region:
                    sample = mff_transform.grid.crop_grid(sample, region)
            except Exception:
                _logger.info(f"FAIL loading {sample_file}")
                if dataset is None:
                    raise
                else:
                    failed += 1
                    continue

            failed = 0
            if variables:
                sample = sample[variables].isel(ocean_time=slice(0, horizon)).squeeze()
            else:
                sample = sample.isel(ocean_time=slice(0, horizon)).squeeze()

            if rename_vars is not None:
                sample = sample.rename(rename_vars)

            if dataset is None:
                dataset = sample
            else:
                dataset = xr.concat([dataset, sample], dim="ocean_time")

            sample.close()
            del sample

        dataset = dataset.sortby("ocean_time")

    return dataset


def load_model_ibi_daily_analysis_local(period="latest", *args, **kwargs):
    """Load IBI daily analysis files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")

    product_info = _config.get("products").get("ibi_daily_analysis")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    if variables:
        model_params = product_info.get("characteristics").get("parameters")
        model_variables = [model_params.get(v, v) for v in variables]
        variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        model_params = product_info.get("characteristics").get("parameters")
        kwargs["rename_vars"] = {}
        for k, v in model_params.items():
            if isinstance(v, str):
                kwargs["rename_vars"][v] = k

    return load_model_ibi(
        period,
        region,
        local_info,
        template_filename,
        variables=variables,
        **kwargs
    )


def load_model_ibi(
    period, region, product_info, template_filename, variables=None, **kwargs
):
    """Load IBI-MFC model."""
    _logger = MFFLog().getLogger(__name__)

    rename_vars = kwargs.pop("rename_vars", None)

    if region:
        _logger.info(f"Loading for subregion: {region}")

    dataset = None

    if "scratch_dir" not in kwargs:
        kwargs["scratch_dir"] = "./scratch_dir"
    mff_utils.patterns.create_dir(kwargs["scratch_dir"])
    _logger.warning(f"IBI .nc.gz files will be uncrompressed at {kwargs['scratch_dir']}")

    if isinstance(period, str):
        period = (period, period)

    if isinstance(period, tuple):
        if period[1] == "latest":
            period[1] = pendulum.now()
        if isinstance(period[0], str):
            start = mff_utils.patterns.parse_date(period[0])
        else:
            start = period[0]
        if isinstance(period[1], str):
            end = mff_utils.patterns.parse_date(period[1])
        else:
            end = period[1]

        period = pendulum.period(end, start)

        for date in tqdm(period.range("days")):
            try:
                sample_file = template_filename.format(date=date, **product_info)

                _logger.info(f"Loading {sample_file}")

                sample = socib_read(sample_file, **kwargs)

                if sample is None:
                    raise ValueError

                if region:
                    coord_dict = dict(x="longitude", y="latitude")
                    sample = mff_transform.grid.crop_grid(sample, region, coord_dict=coord_dict)
            except Exception:
                _logger.info(f"FAIL loading {sample_file}")
                continue

            if variables:
                sample = sample[variables].squeeze()
            else:
                sample = sample.squeeze()

            if rename_vars is not None:
                sample = sample.rename(rename_vars)

            if dataset is None:
                dataset = sample
            else:
                dataset = xr.concat([dataset, sample], dim="time")

            sample.close()
            del sample

        dataset = dataset.sortby("time")

    return dataset
