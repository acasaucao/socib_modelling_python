# -*- coding: UTF-8 -*-

"""Module with methods to process atmospheric models like HARMONIE and ECMWF."""

import pendulum
import xarray as xr
import numpy as np
from datetime import timedelta as td
from tqdm import tqdm

from ..io import MFFLog
from ..io import read as socib_read
from .. import utils as mff_utils
from .. import transform as mff_transform
from ..config import Config as MFF_Config


_MFF_CONFIG = MFF_Config()
_config = _MFF_CONFIG.get("miner")


def load_atmospheric_model(model="harmonie", period="latest", *args, **kwargs):
    """Load a model scenario."""
    _logger = MFFLog().getLogger(__name__)

    if model == "harmonie":
        return load_model_harmonie_local(period, *args, **kwargs)
    elif model == "hirlam_hnr":
        return load_model_hirlam_nhr_local(period, *args, **kwargs)
    elif model == "hirlam_imedea_hnr":
        return load_model_hirlam_imedea_hnr_local(period, *args, **kwargs)
    else:
        _logger.error(f"{model} is not a valid oceanic model!")


def load_model_harmonie_local(period="latest", *args, **kwargs):
    """Load HARMONIE files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 1)
    model_time_span = kwargs.pop("model_time_span", 48)

    product_info = _config.get("products").get("harmonie")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    model_params = product_info.get("characteristics").get("parameters")
    if variables:
        model_variables = []
        for v in variables:
            if v in model_params:
                model_variables.append(model_params[v])
            elif v in model_params.values():
                model_variables.append(v)
        if model_variables:
            variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        rename_vars = {}
        for k, v in model_params.items():
            if (variables is not None) and (v in variables):
                rename_vars[v] = k
        kwargs["rename_vars"] = rename_vars

    return load_model_harmonie(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        local_info,
        template_filename,
        variables=variables,
        **kwargs,
    )


def load_model_harmonie(
    period,
    region,
    time_window,
    model_dt,
    model_time_span,
    product_info,
    template_filename,
    variables=None,
    **kwargs,
):
    """Load WMOP model."""
    _logger = MFFLog().getLogger(__name__)

    kwargs.pop("scratch_dir", None)
    rename_vars = kwargs.pop("rename_vars", None)

    if region:
        _logger.info(f"Loading for subregion: {region}")

    max_failed = model_time_span // time_window
    failed = 0

    dataset = None

    if isinstance(period, str):
        period = (period, period)

    if isinstance(period, tuple):
        if period[1] == "latest":
            now = pendulum.now()
            period = (now, now)
        if isinstance(period[0], str):
            start = mff_utils.patterns.parse_date(period[0])
        else:
            start = period[0]
        if isinstance(period[1], str):
            end = mff_utils.patterns.parse_date(period[1])
        else:
            end = period[1]

        period = pendulum.period(start, end)

        for date in tqdm(period.range("days")):
            if dataset is None:
                horizon = time_window // model_dt
            else:
                horizon = (
                    time_window + (min(failed, max_failed) * time_window)
                ) // model_dt

            hours_failed = 0

            for h in range(horizon):
                try:
                    sample_file = template_filename.format(
                        date=date, h=h, m=0, **product_info
                    )
                    _logger.info(f"Loading {sample_file}")

                    sample = socib_read(sample_file, **kwargs)

                    if sample is None:
                        raise ValueError

                    if region:
                        coord_dict = dict(x="lon", y="lat")
                        sample = mff_transform.grid.crop_grid(
                            sample, region, coord_dict=coord_dict
                        )
                except Exception:
                    _logger.info(f"FAIL loading {sample_file}")
                    if dataset is None:
                        raise
                    else:
                        hours_failed += 1
                        continue

                if variables is not None:
                    sample = sample[variables].squeeze()
                else:
                    sample = sample.squeeze()

                if rename_vars is not None:
                    sample = sample.rename(rename_vars)

                if dataset is None:
                    dataset = sample
                else:
                    dataset = xr.concat([dataset, sample], dim="time")

                sample.close()
                del sample

            if hours_failed == time_window:
                failed += 1
            else:
                failed = 0

    dataset = dataset.sortby("time")

    return dataset


def load_model_hirlam_nhr_local(period="latest", *args, **kwargs):
    """Load HIRLAN NHR files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 1)
    model_time_span = kwargs.pop("model_time_span", 48)

    product_info = _config.get("products").get("hirlam_hnr")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    model_params = product_info.get("characteristics").get("parameters")
    if variables:
        model_variables = []
        for v in variables:
            if v in model_params:
                model_variables.append(model_params[v])
            elif v in model_params.values():
                model_variables.append(v)
        if model_variables:
            variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        rename_vars = {}
        for k, v in model_params.items():
            if (variables is not None) and (v in variables):
                rename_vars[v] = k
        kwargs["rename_vars"] = rename_vars

    return load_model_hirlam_nhr(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        local_info,
        template_filename,
        variables=variables,
        **kwargs,
    )


def load_model_hirlam_imedea_hnr_local(period="latest", *args, **kwargs):
    """Load HIRLAN NHR files from local file systems."""
    _logger = MFFLog().getLogger(__name__)

    region = kwargs.pop("region", None)
    if region:
        _logger.info(f"Loading for subregion: {region}")
    time_window = kwargs.pop("time_window", 24)
    model_dt = kwargs.pop("model_dt", 3)
    model_time_span = kwargs.pop("model_time_span", 48)

    product_info = _config.get("products").get("hirlam_imedea_hnr")
    local_info = product_info["repository"].get("local", {})
    template_filename = local_info.get("template_filename")

    variables = kwargs.pop("variables", None)
    model_params = product_info.get("characteristics").get("parameters")
    if variables:
        model_variables = []
        for v in variables:
            if v in model_params:
                model_variables.append(model_params[v])
            elif v in model_params.values():
                model_variables.append(v)
        if model_variables:
            variables = model_variables

    translate = kwargs.pop("translate", False)
    if translate:
        rename_vars = {}
        for k, v in model_params.items():
            if (variables is not None) and (v in variables):
                rename_vars[v] = k
        kwargs["rename_vars"] = rename_vars

    return load_model_hirlam_nhr(
        period,
        region,
        time_window,
        model_dt,
        model_time_span,
        local_info,
        template_filename,
        variables=variables,
        **kwargs,
    )


def decode_time_hirlam(base, hours):
    base_time = mff_utils.patterns.parse_date(base)
    time = np.datetime64(base_time + td(hours=int(hours)))
    return time


def load_model_hirlam_nhr(
    period,
    region,
    time_window,
    model_dt,
    model_time_span,
    product_info,
    template_filename,
    variables=None,
    **kwargs,
):
    """Load WMOP model."""
    _logger = MFFLog().getLogger(__name__)

    kwargs.pop("scratch_dir", None)
    rename_vars = kwargs.pop("rename_vars", None)

    if region:
        _logger.info(f"Loading for subregion: {region}")

    max_failed = model_time_span // time_window
    failed = 0

    dataset = None

    if isinstance(period, str):
        period = (period, period)

    if isinstance(period, tuple):
        if period[1] == "latest":
            now = pendulum.now()
            period = (now, now)
        if isinstance(period[0], str):
            start = mff_utils.patterns.parse_date(period[0])
        else:
            start = period[0]
        if isinstance(period[1], str):
            end = mff_utils.patterns.parse_date(period[1])
        else:
            end = period[1]

        period = pendulum.period(start, end)

        for date in tqdm(period.range("days")):
            if dataset is None:
                horizon = time_window
            else:
                horizon = (
                    time_window + (min(failed, max_failed) * time_window)
                )

            hours_failed = 0

            for h in range(0, horizon, model_dt):
                try:
                    run_type = "fc"
                    if h == 0:
                        run_type = "an"
                    sample_file = template_filename.format(
                        date=date, h=h, m=0, run=run_type, **product_info
                    )
                    _logger.info(f"Loading {sample_file}")

                    kwargs["decode_times"] = False
                    sample = socib_read(sample_file, **kwargs).isel(record=0)

                    if sample is None:
                        raise ValueError

                    sample["time"].values = decode_time_hirlam(
                        sample.attrs["model_integration_date"],
                        sample.time.values,
                    )
                    sample = sample.set_coords("time")

                    if region:
                        coord_dict = dict(x="longitude", y="latitude")
                        sample = mff_transform.grid.crop_grid(
                            sample, region, coord_dict=coord_dict
                        )
                except Exception:
                    _logger.info(f"FAIL loading {sample_file}")
                    if dataset is None:
                        raise
                    else:
                        hours_failed += 1
                        continue

                if variables is not None:
                    sample = sample[variables].squeeze()
                else:
                    sample = sample.squeeze()

                if rename_vars is not None:
                    sample = sample.rename(rename_vars)

                if dataset is None:
                    dataset = sample
                else:
                    dataset = xr.concat([dataset, sample], dim="time")

                sample.close()
                del sample

            if hours_failed == time_window:
                failed += 1
            else:
                failed = 0

    dataset = dataset.sortby("time")

    return dataset
