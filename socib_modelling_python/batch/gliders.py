# -*- coding: UTF-8 -*-

"""Module with methods to process glider data."""
import re
import datetime
import numpy as np

from .oceanic_models import load_oceanic_model
from ..io import MFFLog, read as socib_read
from .. import utils as mff_utils
from ..miner import (
    get_glider_missions,
    get_metadata_remote_file,
    find_latest_in_remote_files,
)
from ..compute.seawater import press2depth
from ..config import Config as MFF_Config

_MFF_CONFIG = MFF_Config()
_config = _MFF_CONFIG.get("miner")


def compute_glider_transect(mission, models, **kwargs):
    """Compute the transect of a glider mission on a ocean model."""
    metadata = get_metadata_remote_file(mission)
    start = None
    end = None

    if "time_coverage_start" in metadata:
        start = mff_utils.patterns.parse_date(metadata["time_coverage_start"])
        start = start.replace(hour=0, minute=0, second=0)
        end = mff_utils.patterns.parse_date(metadata["time_coverage_end"])
    elif "date_created" in metadata:
        start = mff_utils.patterns.parse_date(metadata["date_created"])
        start = start.replace(hour=0, minute=0, second=0)
        end = start + datetime.timedelta(days=1)

    glider = socib_read(mission)
    glider = glider.assign_attrs({"mission": mission.split("/")[-1][:-3]})

    press = glider["pressure"]
    lats = glider["latitude"]
    rename_dict = {"real_depth": "depth"}
    interp_coord = ["time", "latitude", "longitude", "depth"]

    if "depth" in glider.dims:
        depth_size = glider.dims["depth"]
        lats = np.repeat(glider["latitude"].values[:, np.newaxis], depth_size, axis=1)

    rename_dict = {"depth": "depth_coord", "real_depth": "depth"}
    glider["real_depth"] = press2depth(press, lats)
    glider = glider.rename(rename_dict)

    minlon = glider["longitude"].min()
    maxlon = glider["longitude"].max()
    minlat = glider["latitude"].min()
    maxlat = glider["latitude"].max()
    region = (minlon, maxlon, minlat, maxlat)

    transects = {'glider': glider}
    chunks = kwargs.pop("chunks", {})
    interpolation_method = kwargs.pop("interp_method", "nearest")
    for model in models:
        model_ds = load_oceanic_model(model=model, period=(start, end), region=region,
                                      chunks=chunks.get(model, None), **kwargs)
        model_info = _config.get("products").get(model)
        model_params = model_info.get("characteristics").get("parameters")

        model_interp_coord = {}
        model_interp_coord_list = []
        for k in interp_coord:
            if isinstance(model_params[k], list):
                for param in model_params[k]:
                    if param not in model_ds:
                        continue
                    model_interp_coord[param] = glider[k]
                    model_interp_coord_list.append(param)
            else:
                param = model_params[k]
                if param not in model_ds:
                    continue
                model_interp_coord[param] = glider[k]
                model_interp_coord_list.append(param)

        # model_interp_coord = {model_params[k]: glider[k] for k in interp_coord}
        # model_interp_coord_list = [model_params[k] for k in interp_coord]
        coords_to_drop = set(model_interp_coord_list).difference(interp_coord)

        transects[model] = model_ds.interp(
            method=interpolation_method,
            **model_interp_coord
        ).reset_coords(names=coords_to_drop, drop=True)
        del model_ds

    return transects


def find_pattern_glider(missions, pattern="canales"):
    """Find missions with a pattern on the metadata."""
    filtered = []
    for glider in missions:
        metadata = get_metadata_remote_file(glider)
        try:
            if re.search(
                pattern, metadata["summary"], flags=re.IGNORECASE
            ) or re.search(pattern, metadata["abstract"], flags=re.IGNORECASE):
                filtered.append(glider)
        except KeyError:
            continue
    return filtered


def compute_glider_transect_canales(
    models, period, processing_level=None, data_type="(rt)|(dt)", *args, **kwargs
):
    """Compute the transect of a glider mission on a ocean model."""
    _logger = MFFLog().getLogger(__name__)

    select = []
    if processing_level:
        select.append(f"({processing_level})")
    if data_type:
        select.append(f"({data_type})")

    crawler_kwargs = dict(workers=10, select=select)

    if isinstance(period, tuple):
        if mff_utils.patterns.is_valid_date(
            period[0]
        ) and mff_utils.patterns.is_valid_date(period[1]):
            crawler_kwargs["after"] = mff_utils.patterns.parse_date(period[0])
            crawler_kwargs["before"] = mff_utils.patterns.parse_date(period[1])
        else:
            _logger.error(f"One of {period} is not a valid date.")
            return None
    else:
        _logger.error(
            f"Date {period} is not a valid. It needs a tuple of strings with valid dates"  # noqa
        )
        return None

    missions = get_glider_missions(crawler_kwargs=crawler_kwargs)
    canales = find_pattern_glider(missions)
    if canales:
        canales = find_latest_in_remote_files(canales)
    print(canales)
    transects = compute_glider_transect(canales, models, **kwargs)

    return transects
