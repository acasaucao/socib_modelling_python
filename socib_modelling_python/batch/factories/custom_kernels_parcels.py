# -*- coding: UTF-8 -*-

"""Module defining custom kernels in Parcels format."""

from parcels import rng as random
import math


"""
Created on December 1 2018
@author: Philippe Delandmeter
Kernels defining floating MP particle dynamics
"""


def AdvectionRK4_Beaching(particle, fieldset, time):
    """Advection 2D Runge-Kutta-4 with beaching."""
    if particle.beached == 0:
        (u1, v1) = fieldset.UV[time, particle.depth, particle.lat, particle.lon]
        lon1, lat1 = (
            particle.lon + u1 * 0.5 * particle.dt,
            particle.lat + v1 * 0.5 * particle.dt,
        )

        (u2, v2) = fieldset.UV[time + 0.5 * particle.dt, particle.depth, lat1, lon1]
        lon2, lat2 = (
            particle.lon + u2 * 0.5 * particle.dt,
            particle.lat + v2 * 0.5 * particle.dt,
        )

        (u3, v3) = fieldset.UV[time + 0.5 * particle.dt, particle.depth, lat2, lon2]
        lon3, lat3 = (particle.lon + u3 * particle.dt, particle.lat + v3 * particle.dt)

        (u4, v4) = fieldset.UV[time + particle.dt, particle.depth, lat3, lon3]
        particle.lon += (u1 + 2 * u2 + 2 * u3 + u4) / 6.0 * particle.dt
        particle.lat += (v1 + 2 * v2 + 2 * v3 + v4) / 6.0 * particle.dt
        particle.beached = 2


def AdvectionRK4_3D_Beaching(particle, fieldset, time):
    """Advection 3D Runge-Kutta-4 with beaching."""
    if particle.beached == 0:
        (u1, v1, w1) = fieldset.UVW[time, particle.depth, particle.lat, particle.lon]
        lon1 = particle.lon + u1 * 0.5 * particle.dt
        lat1 = particle.lat + v1 * 0.5 * particle.dt
        dep1 = particle.depth + w1 * 0.5 * particle.dt
        (u2, v2, w2) = fieldset.UVW[time + 0.5 * particle.dt, dep1, lat1, lon1]
        lon2 = particle.lon + u2 * 0.5 * particle.dt
        lat2 = particle.lat + v2 * 0.5 * particle.dt
        dep2 = particle.depth + w2 * 0.5 * particle.dt
        (u3, v3, w3) = fieldset.UVW[time + 0.5 * particle.dt, dep2, lat2, lon2]
        lon3 = particle.lon + u3 * particle.dt
        lat3 = particle.lat + v3 * particle.dt
        dep3 = particle.depth + w3 * particle.dt
        (u4, v4, w4) = fieldset.UVW[time + particle.dt, dep3, lat3, lon3]
        particle.lon += (u1 + 2 * u2 + 2 * u3 + u4) / 6.0 * particle.dt
        particle.lat += (v1 + 2 * v2 + 2 * v3 + v4) / 6.0 * particle.dt
        particle.depth += (w1 + 2 * w2 + 2 * w3 + w4) / 6.0 * particle.dt
        particle.beached = 2


def StokesDrag_Beaching(particle, fieldset, time):
    """Stokes drag with beaching."""
    if particle.lat < 80 and particle.beached == 0:
        (u_uss, v_uss) = fieldset.UVuss[
            time, particle.depth, particle.lat, particle.lon
        ]
        particle.lon += u_uss * particle.dt
        particle.lat += v_uss * particle.dt
        particle.beached = 3


def BrownianMotion2D_Beaching(particle, fieldset, time):
    """Brownian Motion 2D with beaching."""
    if particle.beached == 0:
        kh_meridional = fieldset.Kh_meridional[
            time, particle.depth, particle.lat, particle.lon
        ]
        kh_zonal = fieldset.Kh_zonal[time, particle.depth, particle.lat, particle.lon]
        dx = fieldset.meshSize[time, particle.depth, particle.lat, particle.lon]
        dx0 = 1000

        particle.lat += random.uniform(-1.0, 1.0) * math.sqrt(
            2 * math.fabs(particle.dt) * kh_meridional * math.pow(dx / dx0, 1.33)
        )
        particle.lon += random.uniform(-1.0, 1.0) * math.sqrt(
            2 * math.fabs(particle.dt) * kh_zonal * math.pow(dx / dx0, 1.33)
        )
        particle.beached = 3


def BeachTesting_2D(particle, fieldset, time):
    """Test beaching in 2D."""
    if particle.beached == 2 or particle.beached == 3:
        (u, v) = fieldset.UV[time, particle.depth, particle.lat, particle.lon]
        if math.fabs(u) < 1e-14 and math.fabs(v) < 1e-14:
            if particle.beached == 2:
                particle.beached = 4
            else:
                particle.beached = 1
        else:
            particle.beached = 0


def BeachTesting_3D(particle, fieldset, time):
    """Test beaching in 3D."""
    if particle.beached == 2 or particle.beached == 3:
        (u, v, w) = fieldset.UVW[time, particle.depth, particle.lat, particle.lon]
        if math.fabs(u) < 1e-14 and math.fabs(v) < 1e-14:
            if particle.beached == 2:
                particle.beached = 4
            else:
                particle.beached = 1
        else:
            particle.beached = 0


def UnBeaching(particle, fieldset, time):
    """Unbeach particles."""
    if particle.beached == 4:
        (ub, vb) = fieldset.UVunbeach[time, particle.depth, particle.lat, particle.lon]
        particle.lon += ub * particle.dt
        particle.lat += vb * particle.dt
        particle.beached = 0
        particle.unbeachCount += 1


def Ageing(particle, fieldset, time):
    """Update particle age."""
    particle.age += particle.dt


def DeleteParticle(particle, fieldset, time):
    """Delete a particle."""
    particle.delete()


"""
SOCIB Custom Kernels
"""


def periodicBC(particle, fieldset, time):
    """Execute a period boundary kernel."""
    if particle.lon < fieldset.halo_west:
        particle.lon += fieldset.halo_east - fieldset.halo_west
    elif particle.lon > fieldset.halo_east:
        particle.lon -= fieldset.halo_east - fieldset.halo_west

    if particle.lat < fieldset.halo_south:
        particle.lat += fieldset.halo_north - fieldset.halo_south
    elif particle.lat > fieldset.halo_north:
        particle.lat -= fieldset.halo_north - fieldset.halo_south


def delete_over_surface(particle, fieldset, time):
    """Kernel to keep particles on 0-depth, avoiding Through-surface error."""
    print(
        "Time %g: Particle %d at (%g, %g, %g) is %d"
        % (
            particle.time,
            particle.id,
            particle.lat,
            particle.lon,
            particle.depth,
            particle.state,
        )
    )
    if particle.depth <= 0:
        particle.delete()


def AdvectionRK4_3D_surface_limited(particle, fieldset, time):
    """Run AdvectionRK4_3D kernel keeping particles on 0-depth, avoiding Through-surface error."""
    (u1, v1, w1) = fieldset.UVW[particle]

    lon1 = particle.lon + u1 * 0.5 * particle.dt
    lat1 = particle.lat + v1 * 0.5 * particle.dt
    dep1 = particle.depth + w1 * 0.5 * particle.dt
    dep1 = max(0, dep1)
    (u2, v2, w2) = fieldset.UVW[time + 0.5 * particle.dt, dep1, lat1, lon1, particle]

    lon2 = particle.lon + u2 * 0.5 * particle.dt
    lat2 = particle.lat + v2 * 0.5 * particle.dt
    dep2 = particle.depth + w2 * 0.5 * particle.dt
    dep2 = max(0, dep2)
    (u3, v3, w3) = fieldset.UVW[time + 0.5 * particle.dt, dep2, lat2, lon2, particle]

    lon3 = particle.lon + u3 * particle.dt
    lat3 = particle.lat + v3 * particle.dt
    dep3 = particle.depth + w3 * particle.dt
    dep3 = max(0, dep3)
    (u4, v4, w4) = fieldset.UVW[time + particle.dt, dep3, lat3, lon3, particle]

    particle.lon += (u1 + 2 * u2 + 2 * u3 + u4) / 6.0 * particle.dt
    particle.lat += (v1 + 2 * v2 + 2 * v3 + v4) / 6.0 * particle.dt
    particle.depth += (w1 + 2 * w2 + 2 * w3 + w4) / 6.0 * particle.dt

    particle.depth = max(0, particle.depth)
