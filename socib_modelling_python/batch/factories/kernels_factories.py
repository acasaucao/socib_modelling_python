# -*- coding: UTF-8 -*-

"""Kernels Factories."""

import inspect
from importlib.machinery import SourceFileLoader
import parcels

from . import custom_kernels_parcels


class KernelFactoryParcels(object):
    """docstring for KernelFactoryParcels."""

    def __init__(self, custom_kernels_path=None):
        """Initialize Kernel Factory."""
        super(KernelFactoryParcels, self).__init__()
        self.builtin_kernels = {}
        self.load_builtin_kernels()
        self.builtin_custom_kernels = {}
        self.load_builtin_custom_kernels()
        self.custom_kernels = {}
        if custom_kernels_path is not None:
            self.load_custom_kernels(custom_kernels_path=custom_kernels_path)

    def load_builtin_kernels(self):
        """Load builtin kernels from Parcels."""
        availables = inspect.getmembers(parcels.application_kernels, inspect.isfunction)
        for name, kernel in availables:
            self.builtin_kernels[name] = kernel

    def load_builtin_custom_kernels(self):
        """Load builtin custom kernels from Parcels."""
        availables = inspect.getmembers(custom_kernels_parcels, inspect.isfunction)
        for name, kernel in availables:
            self.builtin_custom_kernels[name] = kernel

    def load_custom_kernels(self, custom_kernels_path=None):
        """Load custom kernels from Parcels a specific file."""
        if isinstance(custom_kernels_path, str):
            custom_module = SourceFileLoader("*", custom_kernels_path).load_module()
            availables = inspect.getmembers(custom_module, inspect.isfunction)
            for name, kernel in availables:
                print(f"Loading Custom Kernel: {name}")
                self.custom_kernels[name] = kernel

    def produce(self, kind, particle_set=None):
        """Produce a kernel."""
        kernel = None
        if kind in self.custom_kernels:
            kernel = self.custom_kernels[kind]
        elif kind in self.builtin_custom_kernels:
            kernel = self.builtin_custom_kernels[kind]
        elif kind in self.builtin_kernels:
            kernel = self.builtin_kernels[kind]

        if particle_set is not None:
            kernel = particle_set.Kernel(kernel)

        return kernel
