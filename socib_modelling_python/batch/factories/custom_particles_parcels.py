# -*- coding: UTF-8 -*-

"""Module defining custom Partciles in Parcels format."""

from parcels import JITParticle, Variable
import numpy as np


class PlasticParticle(JITParticle):
    """Plastic Particle based on Parcels Paper North Sea."""

    age = Variable("age", dtype=np.float32, initial=0.0)
    # beached : 0 sea, 1 beached, 2 after non-beach dyn, 3 after beach dyn, 4 please unbeach
    beached = Variable("beached", dtype=np.int32, initial=0.0)
    unbeachCount = Variable("unbeachCount", dtype=np.int32, initial=0.0)


class JITParticleAssociated(JITParticle):
    """Particle with a set of other partciles associated."""
