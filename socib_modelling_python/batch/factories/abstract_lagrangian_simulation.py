# -*- coding: UTF-8 -*-

"""Abstracted/Base Classes for Lagrangian Simmulations."""

import abc


class Particle(object):
    """docstring for Particles."""

    def __init__(self):
        """Initialize class attributes."""
        super(Particle, self).__init__()

    def __repr__(self):
        """Define repr function."""
        lon = getattr(self, "longitude", None)
        lat = getattr(self, "latitude", None)
        depth = getattr(self, "depth", None)
        time = getattr(self, "time", None)
        return repr(
            f"'longitude': {lon}, 'latitude': {lat}, 'depth': {depth}, 'time': {time}"
        )


class AbstractLagrangianSimulation(object):
    """docstring for AbstractLagrangianSimulation."""

    __metaclass__ = abc.ABCMeta

    def __init__(
        self, particles=[], kernels=[], boundaries_strategy=None, surface_strategy=None
    ):
        """Initialize class attributes."""
        super(AbstractLagrangianSimulation, self).__init__()

    @abc.abstractmethod
    def get_particles(self):
        """Retrieve all particles."""
        return

    @abc.abstractmethod
    def get_particle(self, pid):
        """Retrieve a particle based on particle's id."""
        return

    @abc.abstractmethod
    def get_particle_location(self, pid):
        """Retrieve a particle location based on particle's id."""
        return
