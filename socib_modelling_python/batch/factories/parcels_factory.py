# -*- coding: UTF-8 -*-

"""Factory to produce Parcels simulations."""

import numpy as np
import xarray as xr
import json
from datetime import timedelta
from parcels import FieldSet, ParticleSet, ErrorCode, Field
from .abstract_lagrangian_simulation import AbstractLagrangianSimulation
from .particles_factories import ParticleAbstractFactory
from .kernels_factories import KernelFactoryParcels
from ..generate_particles import GenerateParticles
from ...config import Config as MFF_Config
from ...utils import patterns as mff_patterns

_MFF_CONFIG = MFF_Config()


class ParcelsFactory(object):
    """Factory of Parcels Simulations."""

    def __init__(self):
        """Initialize Parcels Factory."""
        super(ParcelsFactory, self).__init__()

    @classmethod
    def produce(self, specs=None):
        """Produce a Parcels simulation based on a given specification."""
        config = None
        if mff_patterns.is_valid_json(specs):
            with open(specs, "r") as file_specs:
                config = json.load(file_specs)
        elif isinstance(specs, dict):
            config = specs
        elif isinstance(
            specs, str
        ):  # if the configuration is not valid, it loads the default.
            config = _MFF_CONFIG.get(specs, None)

        simulation = ParcelsSimulation(
            custom_kernels_path=config.get("kernels", {}).get("custom_kernels_path")
        )

        kernels_sequence = config.get("kernels", {}).get("sequence", {})
        simulation.load_fieldsets(
            kernels_sequence=kernels_sequence, **config.get("fieldset", {})
        )
        simulation.load_partilces(**config.get("particles", {}))
        simulation.load_strategies(config.get("strategies", {}))
        simulation.load_kernels(**config.get("kernels", {}))
        simulation.config = config.get("execution", {})

        return simulation


class ParcelsSimulation(AbstractLagrangianSimulation):
    """docstring for ParcelsSimulation."""

    def __init__(self, **kwargs):
        """Initialize class attributes."""
        super(ParcelsSimulation, self).__init__()
        self.simulator_type = "parcels"
        self.recovery = {}
        self.kernel_factory = KernelFactoryParcels(kwargs.get("custom_kernels_path"))
        self.particle_factory = ParticleAbstractFactory()

    def load_fieldsets(self, kernels_sequence={}, **kwargs):
        """Load the object FieldSet."""
        params = kwargs.pop("params")
        if "field" not in params:
            raise KeyError("field is a required parameter for fieldsets.")

        field = params.pop("field")
        if isinstance(field, xr.Dataset):
            self.field = FieldSet.from_xarray_dataset(field, **params)

        if "DiffusionUniformKh" in kernels_sequence:
            g = self.field.U.grid
            gshape = g.zdim, g.ydim, g.xdim
            self.field.add_field(
                Field(
                    "Kh_zonal",
                    np.ones(gshape),
                    lon=g.lon,
                    lat=g.lat,
                    depth=g.depth,
                    mesh="spherical",
                    fieldtype="Kh_zonal",
                )
            )
            self.field.add_field(
                Field(
                    "Kh_meridional",
                    np.ones(gshape),
                    lon=g.lon,
                    lat=g.lat,
                    depth=g.depth,
                    mesh="spherical",
                    fieldtype="Kh_meridional",
                )
            )

    def generate_partilces(self, **kwargs):
        """Generate particles."""
        method = kwargs.get("method", "random")
        if method == "random":
            return GenerateParticles.random(**kwargs.get("params", {}))
        elif method == "regular":
            return GenerateParticles.regular(**kwargs.get("params", {}))
        elif method == "at_location":
            return GenerateParticles.at_location(**kwargs.get("params", {}))
        elif method == "from_grid":
            return GenerateParticles.from_grid(**kwargs.get("params", {}))
        elif method == "associated_particles":
            return GenerateParticles.associated_particles(**kwargs.get("params", {}))
        elif method == "from_list":
            return GenerateParticles.from_list(**kwargs.get("params", {}))

        return []

    def load_partilces(self, custom_particles_path=None, **kwargs):
        """Load and construct all particles."""
        lons = None
        lats = None
        depth = None
        time = None

        params = kwargs.get("params", {})
        pclass_config = kwargs.pop("pclass_config", {})

        generated_particles = self.generate_partilces(**kwargs.get("generator", {}))
        if "association" in kwargs["generator"]:
            associations_cfg = kwargs["generator"]["association"]

            if "filename_associations" not in associations_cfg:
                filename_associations = mff_patterns.get_tmp_file(
                    only_name=True, delete=False
                )
                filename_associations = f"{filename_associations}.nc"
            else:
                filename_associations = associations_cfg.pop("filename_associations")

            upd_particles, associations = GenerateParticles.associate_particles(
                generated_particles, **associations_cfg
            )
            associations.to_netcdf(filename_associations)

            self.particles = upd_particles
            self.filename_associations = filename_associations

        if pclass_config.get("associated", False):
            self.particles = generated_particles[0:3]
            associations = generated_particles[3]
            for n in range(np.shape(associations)[1]):
                params[f"neighbour_{n}"] = associations[:, n]
        else:
            self.particles = generated_particles

        if np.shape(self.particles)[0] == 2:
            lats = self.particles[0]
            lons = self.particles[1]
        elif np.shape(self.particles)[0] == 3:
            lats = self.particles[0]
            lons = self.particles[1]
            depth = self.particles[2]
        elif np.shape(self.particles)[0] == 4:
            lats = self.particles[0]
            lons = self.particles[1]
            depth = self.particles[2]
            time = self.particles[3]  # noqa
        else:
            raise ValueError(
                "Particles should be a list of tuples with 2 to 4 floats each."
            )

        lonlatdepth_dtype = params.pop("lonlatdepth_dtype", None)

        if isinstance(custom_particles_path, str):
            self.particle_factory.load_custom_particles(custom_particles_path)
        pclass = self.particle_factory.produce(
            kwargs.pop("pclass", "JITParticle"), **pclass_config
        )

        repeatdt = params.pop("repeatdt", None)
        if isinstance(repeatdt, dict):
            repeatdt = timedelta(**repeatdt)

        self.particle_set = ParticleSet.from_list(
            self.field,
            pclass=pclass,
            lon=lons,
            lat=lats,
            depth=depth,
            time=time,
            repeatdt=repeatdt,
            lonlatdepth_dtype=lonlatdepth_dtype,
            **params,
        )

    def load_kernels(self, sequence=[], custom_kernels_path=None, **kwargs):
        """Load and construct all kernels."""
        self.kernels = None

        for k in sequence:
            new_kernel = self.kernel_factory.produce(k, particle_set=self.particle_set)
            if (new_kernel is not None) and (self.kernels is None):
                self.kernels = new_kernel
            elif new_kernel is not None:
                self.kernels += new_kernel

        if "DiffusionUniformKh" in sequence:
            diffusivity = kwargs.get("diffusivity", 1)
            if isinstance(diffusivity, tuple):
                self.field.Kh_meridional.data *= diffusivity[0]
                self.field.Kh_zonal.data *= diffusivity[1]
            else:
                self.field.Kh_meridional.data *= diffusivity
                self.field.Kh_zonal.data *= diffusivity

    def __boundaries_strategy(self, boundaries_strategy="DeleteParticle"):
        if boundaries_strategy in [
            "periodic_zonal",
            "periodic_meridional",
            "periodic",
        ]:
            flags = {}
            if boundaries_strategy in ["periodic_zonal", "periodic"]:
                self.field.add_constant("halo_west", self.field.U.grid.lon[0])
                self.field.add_constant("halo_east", self.field.U.grid.lon[-1])
                flags["zonal"] = True
            if boundaries_strategy in ["periodic_meridional", "periodic"]:
                self.field.add_constant("halo_south", self.field.V.grid.lat[0])
                self.field.add_constant("halo_north", self.field.V.grid.lat[-1])
                flags["meridional"] = True
            self.field.add_periodic_halo(**flags)
        else:
            self.recovery[ErrorCode.ErrorOutOfBounds] = self.kernel_factory.produce(
                boundaries_strategy
            )

    def __surface_strategy(self, surface_strategy="DeleteParticle"):
        self.recovery[ErrorCode.ErrorThroughSurface] = self.kernel_factory.produce(
            surface_strategy
        )

    def load_strategies(self, strategies):
        """Sets-up different simulation strategies."""
        for strategy, value in strategies.items():
            if strategy == "boundaries":
                self.__boundaries_strategy(value)
            elif strategy == "surface":
                self.__boundaries_strategy(value)

    def get_particles(self):
        """Return all particles."""
        return self.particle_set

    def get_particle(self, pid):
        """Return a particles based on particle's id."""
        return self.particle_set[pid]

    def get_all_particles_location(self):
        """Return the location of all particles."""
        return self.particles

    def get_particle_location(self, pid):
        """Return a particles location based on particle's id."""
        particle = self.get_particle(pid)
        location = {}
        if isinstance(particle, tuple):
            location["longitude"] = particle[0]
            location["latitude"] = particle[1]
        else:
            location["longitude"] = particle.__getattr__("lon")
            location["latitude"] = particle.__getattr__("lat")
            location["depth"] = particle.__getattr__("depth")
            location["time"] = particle.__getattr__("time")

        return location

    def run(self, output_filename=None):
        """Execute particle simulation."""
        runtime = self.config.get("runtime", None)
        if isinstance(runtime, dict):
            runtime = timedelta(**runtime)
        dt = self.config.get("dt", None)
        if isinstance(dt, dict):
            dt = timedelta(**dt)
        outputdt = self.config.get("outputdt", None)
        if isinstance(outputdt, dict):
            outputdt = timedelta(**outputdt)

        if output_filename is None:
            output_filename = mff_patterns.get_tmp_file(only_name=True, delete=False)
            output_filename = f"{output_filename}.nc"
        output_file = self.particle_set.ParticleFile(
            name=output_filename, outputdt=outputdt
        )

        self.particle_set.execute(
            self.kernels,
            runtime=runtime,
            dt=dt,
            output_file=output_file,
            recovery=self.recovery,
        )

        output_file.close()
        return output_filename
