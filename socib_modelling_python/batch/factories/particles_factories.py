# -*- coding: UTF-8 -*-

"""Module with factories to constuct Particles."""

import inspect
from importlib.machinery import SourceFileLoader
import parcels
from . import custom_particles_parcels
import parcels.particle
from parcels import Variable
import numpy as np


class ParticleFactoryParcels(object):
    """docstring for ParticleFactoryParcels."""

    def __init__(self):
        """Initialize ParticleFactoryParcels."""
        super(ParticleFactoryParcels, self).__init__()
        self.builtin_particles = {}
        self.load_builtin_particles()

    def load_builtin_particles(self):
        """Load builtin particles from Parcels."""
        availables = inspect.getmembers(parcels.particle, inspect.isclass)
        for name, particle in availables:
            if issubclass(particle, parcels.particle._Particle):
                self.builtin_particles[name] = particle

    def produce(self, kind, **kwargs):
        """Produce a built-in particle from Parcels."""
        return self.builtin_particles[kind]


class CustomParticleFactoryParcels(object):
    """docstring for CustomParticleFactoryParcels."""

    def __init__(self, custom_particles={}):
        """Initialize CustomParticleFactoryParcels."""
        super(CustomParticleFactoryParcels, self).__init__()
        self.custom_particles = custom_particles
        self.load_builtin_custom_particles()

    def load_builtin_custom_particles(self):
        """Load builtin custom particles from Parcels."""
        availables = inspect.getmembers(custom_particles_parcels, inspect.isclass)
        for name, particle in availables:
            if issubclass(particle, parcels.particle._Particle):
                self.custom_particles[name] = particle

    def produce(self, kind, **kwargs):
        """Produce a custom particle compatible with Parcels."""
        return self.custom_particles[kind]


class ParcelsJITParticleAssociatedFactory(object):
    """docstring for ParcelsJITParticleAssociatedFactory."""

    def __init__(self):
        """Initialize ParcelsJITParticleAssociatedFactory."""
        super(ParcelsJITParticleAssociatedFactory, self).__init__()

    def produce(self, **kwargs):
        """Produce a custom JIT particle with N associations (neighbours)."""
        particle = custom_particles_parcels.JITParticleAssociated
        return particle


class ParticleAbstractFactory(object):
    """docstring for ParticleFactoryParcels."""

    def __init__(self):
        """Initialize Particle Factory."""
        super(ParticleAbstractFactory, self).__init__()
        self.builtin_particles = {}
        self.load_builtin_particles()
        self.builtin_custom_particles = {}
        self.load_builtin_custom_particles()

    def load_builtin_particles(self):
        """Load builtin particles from Parcels."""
        availables = inspect.getmembers(parcels.particle, inspect.isclass)
        for name, particle in availables:
            if issubclass(particle, parcels.particle._Particle):
                self.builtin_particles[name] = particle

    def load_builtin_custom_particles(self):
        """Load builtin custom particles from Parcels."""
        availables = inspect.getmembers(custom_particles_parcels, inspect.isclass)
        for name, particle in availables:
            if issubclass(particle, parcels.particle._Particle):
                self.builtin_custom_particles[name] = particle

    def load_custom_particles(self, custom_particles_path=None):
        """Load custom particles from Parcels a specific file."""
        if isinstance(custom_particles_path, str):
            custom_module = SourceFileLoader("*", custom_particles_path).load_module()
            availables = inspect.getmembers(custom_module, inspect.isclass)
            for name, particle in availables:
                if issubclass(particle, parcels.particle._Particle):
                    self.builtin_custom_particles[name] = particle

    def produce(self, kind, **kwargs):
        """Produce a kernel."""
        particle = None
        if kind in self.builtin_particles:
            particle = ParticleFactoryParcels().produce(kind, **kwargs)
        elif kind in self.builtin_custom_particles:
            if kind == "JITParticleAssociated":
                particle = ParcelsJITParticleAssociatedFactory().produce(**kwargs)
            else:
                particle = CustomParticleFactoryParcels(
                    custom_particles=self.builtin_custom_particles
                ).produce(kind, **kwargs)

        if kwargs.get("associated", False):
            n_associations = kwargs.get("n_associations", 0)
            for i in range(n_associations):
                key = f"neighbour_{i}"
                value = Variable(key, dtype=np.float32, to_write="once")
                setattr(particle, key, value)

        return particle
