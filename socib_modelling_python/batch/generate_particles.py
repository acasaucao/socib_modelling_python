# -*- coding: UTF-8 -*-

"""Module generate set of particles positions."""
import warnings
import numpy as np
import xarray as xr
from scipy.interpolate import interpn
from itertools import product
from joblib import Parallel, delayed

from ..utils import geodesic as mff_geodesic
from ..config import Config as mff_config
from ..io import MFFLog


class GenerateParticles(object):
    """docstring for GenerateParticles."""

    _logger = MFFLog().getLogger(__name__)

    def __init__(self):
        """Initialize class attributes."""
        super(GenerateParticles, self).__init__()

    @classmethod
    def filter_on_land(
        cls, particles, land_mask, land_mask_coords, return_filter=False
    ):
        """
        Filter all particles on land.

        Parameters
        ----------
        particles : ndarray of floats
            A array of floats with the latitude, longitude, and depths
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.
        return_filter :  bool
            Flag to indicate the return of the excluded indices

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
           None of these particles are on land.
           If return_filter is True an extra list is returned indicating the excluded indices.
        """
        values = []
        for coord in ["depth", "latitude", "longitude"]:
            if coord not in land_mask_coords:
                continue
            translated_coord = land_mask_coords.get(coord, coord)
            values.append(land_mask[translated_coord].values)
        if len(np.shape(land_mask)) == 3:
            onland = interpn(
                values,
                land_mask.values,
                list(zip(particles[2], particles[0], particles[1])),
            )
        elif len(np.shape(land_mask)) == 2:
            onland = interpn(
                values, land_mask.values, list(zip(particles[0], particles[1]))
            )

        onland = np.where(onland == 1, True, False)

        if return_filter:
            return (
                particles[0][~onland],
                particles[1][~onland],
                particles[2][~onland],
                onland,
            )

        return particles[0][~onland], particles[1][~onland], particles[2][~onland]

    @classmethod
    def from_list(
        cls,
        latitudes,
        longitudes,
        depth=None,
        time=None,
        associations=None,
        land_mask=None,
        land_mask_coords=None,
    ):
        """
        Generate a set of particles from a set of lists.

        It just passthru the list of coordinates keeping the particles data type and format.

        Parameters
        ----------
        latitudes : list of floats
           List of particles' latitude.
        longitudes : list of floats
            List of particles' longitude.
        depth : list of floats
            List of particles' depth.
        time : list of floats
            List of timestamps to release each particle.
        associations : numpy array
            Variable containing particles associations.

        Returns
        -------
        latitude, longitude, depth, time : list.
           Returns up-to four lists containing the Particles latitude, longitude, depth, and time.
        """
        particles_latitude = np.array(latitudes)
        particles_longitude = np.array(longitudes)

        payload = (particles_latitude, particles_longitude)

        if depth is None:
            particles_depth = np.ones_like(particles_latitude) * 0

        payload = (particles_latitude, particles_longitude, depth)

        if time is not None:
            payload = payload + (time,)

        if associations is not None:
            payload = payload + (associations,)

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
                return_filter=True,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]
            onland = particles[3]

            payload = (particles_latitude, particles_longitude, particles_depth)

            if len(payload) > 3:
                payload = payload + (payload[3][~onland],)
            if len(payload) > 4:
                payload = payload + (payload[4][~onland],)

        return payload

    @classmethod
    def random(
        cls,
        n_particles,
        lat_limits=None,
        lon_limits=None,
        depth_limits=None,
        region=None,
        land_mask=None,
        land_mask_coords=None,
        random_state=0,
    ):
        """
        Generate a set of particles randomly spread.

        Parameters
        ----------
        n_particles : int
           Total number of particles to be generated.
        lat_limits : (float, float)
            Minimum and Maximum latitude values for the random dispersion.
        lon_limits : (float, float)
            Minimum and Maximum longitude values for the random dispersion.
        depth_limits : (float, float)
            Minimum and Maximum depth values for the random dispersion
            The default value is zero to facilitate surface-only simulations.
        region : str
            Region identifier used to get the area limmits.
            It is the same identifier registered on the Config's database 'regions'.
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.
        random_state : int
            Seed used to initialize the random number generator.

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
        """
        if region is not None:
            if (lat_limits is not None) or (lon_limits is not None):
                cls._logger.warn(
                    "When 'region' and geographical limits are defined, the last one is ignored."
                )
            region = mff_config().get("regions").get(region, {})
            area = region.get("area", [None, None, None, None])
            lon_limits = (area[0], area[1])
            lat_limits = (area[2], area[3])

        if lat_limits is None:
            lat_limits = (-90, 90)
        if lon_limits is None:
            lon_limits = (-180, 180)
        if depth_limits is None:
            depth_limits = (0, 0)

        rng = np.random.default_rng(random_state)

        particles_latitude = rng.uniform(lat_limits[0], lat_limits[1], n_particles)
        particles_longitude = rng.uniform(lon_limits[0], lon_limits[1], n_particles)
        particles_depth = rng.uniform(depth_limits[0], depth_limits[1], n_particles)

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

            valid_particles = len(particles_latitude)

            while valid_particles < n_particles:
                new_n_particles = n_particles - valid_particles
                extra_latitude = rng.uniform(
                    lat_limits[0], lat_limits[1], new_n_particles
                )
                extra_longitude = rng.uniform(
                    lon_limits[0], lon_limits[1], new_n_particles
                )
                extra_depth = rng.uniform(
                    depth_limits[0], depth_limits[1], new_n_particles
                )

                particles = cls.filter_on_land(
                    (extra_latitude, extra_longitude, extra_depth),
                    land_mask,
                    land_mask_coords,
                )
                if particles[0].size == 0:
                    break

                particles_latitude = np.append(particles_latitude, particles[0])
                particles_longitude = np.append(particles_longitude, particles[1])
                particles_depth = np.append(particles_depth, particles[2])

                valid_particles = len(particles_latitude)

        return particles_latitude, particles_longitude, particles_depth

    @classmethod
    def at_location(
        cls,
        n_particles,
        latitude,
        longitude,
        depth=0,
        dispersion=None,
        land_mask=None,
        land_mask_coords=None,
        random_state=0,
    ):
        """
        Generate a set of particles at a specific location.

        Parameters
        ----------
        n_particles : int
           Total number of particles to be generated.
        latitude : float
            Latitude location where the particles are referenced.
        longitude : float
            Longitude location where the particles are referenced.
        depth: float
            Depth location where the particles are referenced.
            The default value is zero to facilitate surface-only simulations.
        dispersion : float
            This is a dispersion factor around latitude and longitude values.
            For each particle and each coordinate separately,
            a random value between 0 and dispersion is add to the coordinate.
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.
        random_state : int
            Seed used to initialize the random number generator.

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
        """
        particles_latitude = np.array([], dtype=object)
        particles_longitude = np.array([], dtype=object)
        particles_depth = np.array([], dtype=object)

        if n_particles == 1:
            return (
                np.array([latitude], dtype=object),
                np.array([longitude], dtype=object),
                np.array([depth], dtype=object),
            )
        elif dispersion is None:
            particles_latitude = np.ones(n_particles) * latitude
            particles_longitude = np.ones(n_particles) * longitude
            particles_depth = np.ones(n_particles) * depth
        else:
            lat_factor, lon_factor = mff_geodesic.meters_to_degrees(
                dispersion, latitude
            )
            rng = np.random.default_rng(random_state)

            random_noise = rng.uniform(-1, 1, n_particles) * lat_factor
            particles_latitude = (np.ones(n_particles) * latitude) + random_noise
            random_noise = rng.uniform(-1, 1, n_particles) * lon_factor
            particles_longitude = (np.ones(n_particles) * longitude) + random_noise
            particles_depth = np.ones(n_particles) * depth

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

            valid_particles = len(particles_latitude)

            if valid_particles == 0:
                msg = "No valid particles generated with land mask activated.\n"
                msg += "Check if location:\n"
                msg += (
                    "Lat = {latitude}, Lon = {longitude}, Depth = {depth} is on land."
                )

                warnings.warn(msg)
                return [], [], []

            while valid_particles < n_particles:
                new_n_particles = n_particles - valid_particles
                extra = cls.at_location(
                    new_n_particles,
                    latitude=latitude,
                    longitude=longitude,
                    depth=depth,
                    dispersion=dispersion,
                    random_state=random_state,
                )

                particles = cls.filter_on_land(extra, land_mask, land_mask_coords)
                if particles[0].size == 0:
                    break
                particles_latitude = np.append(particles_latitude, particles[0])
                particles_longitude = np.append(particles_longitude, particles[1])
                particles_depth = np.append(particles_depth, particles[2])

                valid_particles = len(particles_latitude)

        return particles_latitude, particles_longitude, particles_depth

    @classmethod
    def regular(
        cls,
        lat_limits=None,
        lat_separation=100,
        lon_limits=None,
        lon_separation=100,
        depth_limits=None,
        depth_separation=10,
        region=None,
        land_mask=None,
        land_mask_coords=None,
    ):
        """
        Generate a set of particles regularly spread.

        Parameters
        ----------
        lat_limits : (float, float)
            Minimum and Maximum latitude values for the regular dispersion.
        lat_separation : float
            The regular distance in meters between particles for the latitudinal axis.
        lon_limits : (float, float, float)
            Minimum and Maximum longitude values for the regular dispersion.
        lon_separation : float
            The regular distance in meters between particles for the longitudinal axis.
        depth_limits : (float, float, float)
            Minimum and Maximum depth values for the regular dispersion.
            The default value is zero to facilitate surface-only simulations.
        depth_separation : float
            The regular vertical distance between particles (meters).
        region : str
            Region identifier used to get the area limmits.
            It is the same identifier registered on the Config's database 'regions'.
            When it is not None, the other limits are ignored.
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
        """
        if region is not None:
            if (lat_limits is not None) or (lon_limits is not None):
                cls._logger.warn(
                    "When 'region' and geographical limits are defined, the last one is ignored."
                )
            region = mff_config().get("regions").get(region, {})
            area = region.get("area", [None, None, None, None])
            lon_limits = (area[0], area[1])
            lat_limits = (area[2], area[3])

        if lat_limits is None:
            lat_limits = (-90, 90)
        if lon_limits is None:
            lon_limits = (-180, 180)
        if depth_limits is None:
            depth_limits = (0, 0)

        particles_latitude = np.array([], dtype=object)
        particles_longitude = np.array([], dtype=object)
        particles_depth = np.array([], dtype=object)

        lat_factor, _ = mff_geodesic.meters_to_degrees(lat_separation, 0)
        latitude_reference = np.arange(lat_limits[0], lat_limits[1] + 1e-9, lat_factor)
        depth_reference = np.arange(
            depth_limits[0], depth_limits[1] + 1e-9, depth_separation
        )

        for lat, depth in product(latitude_reference, depth_reference):
            _, lon_factor = mff_geodesic.meters_to_degrees(lon_separation, lat)
            longitude_base = np.arange(lon_limits[0], lon_limits[1] + 1e-9, lon_factor)
            latitude_base = np.ones(len(longitude_base)) * float(lat)
            depth_base = np.ones(len(longitude_base)) * float(depth)
            particles_longitude = np.append(particles_longitude, longitude_base, axis=0)
            particles_latitude = np.append(particles_latitude, latitude_base, axis=0)
            particles_depth = np.append(particles_depth, depth_base)

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

        return particles_latitude, particles_longitude, particles_depth

    @classmethod
    def regular_grid(
        cls,
        reference,
        shape=1000,
        lat_separation=100,
        lon_separation=100,
        depth_separation=10,
        land_mask=None,
        land_mask_coords=None,
    ):
        """
        Generate a set of particles regularly spread.

        Parameters
        ----------
        reference : (float, float, float)
            Coordinates of the point of reference. From this points a regular grid of particles
            will be created upwardly, eastwardly, and to the bottom direction when in 3D mode.
        shape : (float, float, float)
            A tuple of floats indicating the number of particles individually
            for each direction. It can also be interpreted as the size of the grid dimensions.
        lat_separation : float
            The regular distance in meters between particles for the latitudinal axis.
        lon_separation : float
            The regular distance in meters between particles for the longitudinal axis.
        depth_separation : float
            The regular vertical distance between particles (meters).
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
        """
        start_latitude, start_longitude, *start_depth = reference
        start_depth = start_depth[0] if start_depth else 0

        n_particles_lat, n_particles_lon, *n_particles_depth = shape
        n_particles_depth = n_particles_depth[0] if n_particles_depth else 1

        particles_latitude = np.array([], dtype=np.float32)
        particles_longitude = np.array([], dtype=np.float32)
        particles_depth = np.array([], dtype=np.float32)

        lat_factor, _ = mff_geodesic.meters_to_degrees(lat_separation, 0)
        latitude_reference = (np.ones(n_particles_lat) * start_latitude) + (
            np.arange(n_particles_lat) * lat_factor
        )

        depth_reference = (np.ones(n_particles_depth) * start_depth) + (
            np.arange(n_particles_depth) * depth_separation
        )

        for lat, depth in product(latitude_reference, depth_reference):
            _, lon_factor = mff_geodesic.meters_to_degrees(lon_separation, lat)
            longitude_base = (np.ones(n_particles_lon) * start_longitude) + (
                np.arange(n_particles_lon) * lon_factor
            )
            latitude_base = np.ones(len(longitude_base)) * float(lat)
            depth_base = np.ones(len(longitude_base)) * float(depth)
            particles_longitude = np.append(particles_longitude, longitude_base, axis=0)
            particles_latitude = np.append(particles_latitude, latitude_base, axis=0)
            particles_depth = np.append(particles_depth, depth_base)

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

        return particles_latitude, particles_longitude, particles_depth

    @classmethod
    def from_grid(
        cls,
        grid,
        latitude="latitude",
        longitude="longitude",
        depth=None,
        land_mask=None,
        land_mask_coords=None,
        region=None,
    ):
        """
        Generate a set of particles matching the grid point locations.

        Parameters
        ----------
        grid : Xarray dataset.
            Grid dataset
        latitude : object
            Name or index to access the latitude values of the grid.
        longitude : object
            Name or index to access the longitude values of the grid.
        depth : object
            Name or index to access the depth values of the grid.
            Default value is None which means 0 depths for all particles.
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.
        region : str or 4 floats (list, tuple, array...)
            Region identifier used to get the area limmits.
            It is the same identifier registered on the Config's database 'regions'.
            Can also be 4 floats indicating [lon_min, lon_max, lat_min, lat_max]

        Returns
        -------
        latitude, longitude, depth : list.
           Returns three lists containing the Particles latitude, longitude, and depth.
        """
        particles_latitude = np.array([], dtype=object)
        particles_longitude = np.array([], dtype=object)
        particles_depth = None

        grid_lat = grid[latitude]
        grid_lon = grid[longitude]

        area_limits = [
            np.nanmin(grid_lon),
            np.nanmax(grid_lon),
            np.nanmin(grid_lat),
            np.nanmax(grid_lat),
        ]

        if region is not None:
            if isinstance(region, str):
                region = mff_config().get("regions").get(region, {})
                area_limits = region.get("area", area_limits)
            else:
                area_limits = region

        lon_limits = (area_limits[0], area_limits[1])
        lat_limits = (area_limits[2], area_limits[3])

        grid_lon = grid_lon[(grid_lon >= lon_limits[0]) & (grid_lon <= lon_limits[1])]
        grid_lat = grid_lat[(grid_lat >= lat_limits[0]) & (grid_lat <= lat_limits[1])]

        for lat in grid_lat:
            latitude_base = np.ones(len(grid_lon)) * float(lat)
            particles_longitude = np.append(particles_longitude, grid_lon, axis=0)
            particles_latitude = np.append(particles_latitude, latitude_base, axis=0)

        if depth is not None:
            particles_depth = np.tile(grid[depth], len(particles_longitude))
        else:
            particles_depth = np.zeros(np.shape(particles_latitude))

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

        return particles_latitude, particles_longitude, particles_depth

    @classmethod
    def _get_particle_associations(
        cls,
        latitudes,
        longitudes,
        depths,
        pid,
        n_associations,
        distance,
        land_mask=None,
        land_mask_coords={},
    ):
        associations = np.ones((n_associations, 3)) * np.nan
        directions = np.linspace(0, 360, n_associations, endpoint=False)
        position = (latitudes[pid], longitudes[pid], depths[pid])
        aid = 0
        for bearing in directions:
            new_association = mff_geodesic.find_destination(position, distance, bearing)
            # print(position, distance, bearing, new_association)
            on_land = False
            if land_mask is not None:
                interp_coords = {
                    land_mask_coords.get("latitude", "latitude"): new_association[0],
                    land_mask_coords.get("longitude", "longitude"): new_association[1],
                }
                if "depth" in land_mask_coords:
                    interp_coords[
                        land_mask_coords.get("depth", "depth")
                    ] = new_association[2]
                on_land = land_mask.astype(np.int).interp(**interp_coords)
            if not on_land:
                associations[aid] = new_association
                aid += 1

        return associations

    @classmethod
    def associate_particles(
        cls,
        particles,
        n_associations,
        distance,
        land_mask=None,
        land_mask_coords={},
        n_jobs=-1,
    ):
        """
        Associte particle with the 4 nearest neighbours.

        Parameters
        ----------
        particles : ndarray of floats
            A array of floats with the latitude, longitude, and depths
        n_associations : int
            Number of particles' associations
        distance : float
            The distance between a particles and its associated particles.
        land_mask : xarray dataarray
            A DataArray containing a land mask.
        land_mask_coords : dict
            A dictionary translating the coordinates for latitude, longitude, and depth
            inside the land_mask dataarray.
        n_jobs : int
            Number of parrallel process to dispatch.
            Whe n_jobs equal -1, the number of jobs are equal to the number of cpus cores.

        Returns
        -------
        new_particles and associations_index: array, xr.DataArray.
           Returns a updated set of particles.
           This new set contains the original particles and the associated particles.
           Also, a xarray DataArray containing the information about the associations is returned.
           For example, the i-th position of the association list has N indices of N particles
           associated with the particle i. Each particle can have up-to N positive indices.
           A negative index indicates that no more associations are available for that particle.
        """
        latitudes, longitudes, *depths = particles
        depths = depths[0] if depths else None

        if depths is None:
            depths = np.zeros_like(latitudes)

        n_particles = len(latitudes)
        associations_index = np.ones((n_particles, n_associations), dtype=np.float) * -1

        particles_associations = Parallel(n_jobs=n_jobs)(
            delayed(cls._get_particle_associations)(
                latitudes,
                longitudes,
                depths,
                pid,
                n_associations,
                distance,
                land_mask,
                land_mask_coords,
            )
            for pid in range(n_particles)
        )
        particles_associations = np.array(particles_associations)

        counter_valid_associations = 0
        for pid, associations in enumerate(particles_associations):
            valid_associations = associations[~np.isnan(associations).any(axis=-1)]
            for vid, valid in enumerate(valid_associations):
                aid = counter_valid_associations + n_particles
                associations_index[pid][vid] = aid
                counter_valid_associations += 1

        # removing nans
        mask = ~np.isnan(particles_associations).any(axis=-1)
        particles_associations = particles_associations[mask]
        n_associated = np.shape(particles_associations)[0]

        ds_associations = xr.Dataset(
            data_vars=dict(
                associations=(["particle", "association"], associations_index),
                particles=(
                    ["particle", "location"],
                    np.column_stack((latitudes, longitudes, depths)),
                ),
                associated=(
                    ["particles_associated", "location"],
                    particles_associations,
                ),
            ),
            coords=dict(
                particle_id=(["particle"], np.arange(0, n_particles, dtype=np.int)),
                associated_id=(
                    ["particles_associated"],
                    np.arange(n_particles, n_particles + n_associated, dtype=np.int),
                ),
            ),
        )

        # updating particles set
        latitudes = np.append(latitudes, particles_associations[:, 0])
        longitudes = np.append(longitudes, particles_associations[:, 1])
        depths = np.append(depths, particles_associations[:, 2])
        new_particles = (latitudes, longitudes, depths)

        return new_particles, ds_associations

    @classmethod
    def associated_4NN2D(cls, latitudes, longitudes, shape):
        """
        Associte particle with the 4 nearest neighbours.

        Parameters
        ----------
        latitudes : array of floats
            Particles latitude.
        longitudes : object
            Particles longitude
        shape : tuple of int
            Grid shape
        Returns
        -------
        association_list: list.
           Returns a 2 dimensional list containing the particles and their association.
           For example, the i-th position of the association list has N indices of N particles
           associated with the particle i.
        """
        n_particles = len(latitudes)
        associations = []
        for p in range(n_particles):
            p_associations = []
            ip, jp = np.unravel_index(p, shape)
            for i in [-1, 1]:
                ineighbour = ip + i
                new_neighbour = -1
                if (ineighbour >= 0) and (ineighbour < shape[0]):
                    neighbour = np.ravel_multi_index((ineighbour, jp), shape)
                    if neighbour != p:
                        new_neighbour = neighbour
                p_associations.append(new_neighbour)
            for j in [-1, 1]:
                jneighbour = jp + j
                new_neighbour = -1
                if (jneighbour >= 0) and (jneighbour < shape[1]):
                    neighbour = np.ravel_multi_index((ip, jneighbour), shape)
                    if neighbour != p:
                        new_neighbour = neighbour
                p_associations.append(new_neighbour)
            p_associations.sort(reverse=True)
            associations.append(p_associations)
        return np.array(associations)

    @classmethod
    def associated_8NN2D(cls, latitudes, longitudes, shape):
        """
        Associte particle with the 8 nearest neighbours.

        Parameters
        ----------
        latitudes : array of floats
            Particles latitude.
        longitudes : object
            Particles longitude
        shape : tuple of int
            Grid shape
        Returns
        -------
        association_list: list.
           Returns a 2 dimensional list containing the particles and their association.
           For example, the i-th position of the association list has N indices of N particles
           associated with the particle i.
        """
        n_particles = len(latitudes)
        associations = []
        for p in range(n_particles):
            p_associations = []
            ip, jp = np.unravel_index(p, shape)
            for i in [-1, 0, 1]:
                ineighbour = ip + i
                for j in [-1, 0, 1]:
                    if i == 0 and j == 0:
                        continue
                    jneighbour = jp + j
                    cond_i = (ineighbour >= 0) and (ineighbour < shape[0])
                    cond_j = (jneighbour >= 0) and (jneighbour < shape[1])
                    new_neighbour = -1
                    if cond_i and cond_j:
                        neighbour = np.ravel_multi_index(
                            (ineighbour, jneighbour), shape
                        )
                        if neighbour != p:
                            new_neighbour = neighbour
                    p_associations.append(new_neighbour)
            p_associations.sort(reverse=True)
            associations.append(p_associations)
        return np.array(associations)

    @classmethod
    def associated_particles(
        cls,
        grid,
        latitude="latitude",
        longitude="longitude",
        depth=None,
        strategy="4NN2D",
        land_mask=None,
        land_mask_coords=None,
        region=None,
    ):
        """
        Generate a set of associated particles.

        Parameters
        ----------
        grid : Xarray dataset.
            Grid dataset
        latitude : object
            Name or index to access the latitude values of the grid.
        longitude : object
            Name or index to access the longitude values of the grid.
        depth : object
            Name or index to access the depth values of the grid.
            Default value is None which means 0 depths for all particles.
        strategy: str
            Strategy used to associate the particles.
        land_mask : xarray dataarray
            A matrix indicating the land positions
        land_mask_coords : list of str
            list of coordinates names of the land mask.
        region : str or 4 floats (list, tuple, array...)
            Region identifier used to get the area limmits.
            It is the same identifier registered on the Config's database 'regions'.
            Can also be 4 floats indicating [lon_min, lon_max, lat_min, lat_max]

        Returns
        -------
        latitude, longitude, depth, association_list: list.
           Returns four lists with the first three: the Particles latitude, longitude, depth.
           The last list is a 2 dimensional list containing the particles and their association.
           For example, the i-th position of the association list has N indices of N particles
           associated with the particle i.
        """
        particles_latitude = np.array([], dtype=object)
        particles_longitude = np.array([], dtype=object)
        particles_depth = None
        particles_associations = np.array([], dtype=object)

        grid_lat = grid[latitude]
        grid_lon = grid[longitude]

        area_limits = [
            np.nanmin(grid_lon),
            np.nanmax(grid_lon),
            np.nanmin(grid_lat),
            np.nanmax(grid_lat),
        ]

        if region is not None:
            if isinstance(region, str):
                region = mff_config().get("regions").get(region, {})
                area_limits = region.get("area", area_limits)
            else:
                area_limits = region

        lon_limits = (area_limits[0], area_limits[1])
        lat_limits = (area_limits[2], area_limits[3])

        grid_lon = grid_lon[(grid_lon >= lon_limits[0]) & (grid_lon <= lon_limits[1])]
        grid_lat = grid_lat[(grid_lat >= lat_limits[0]) & (grid_lat <= lat_limits[1])]

        for lat in grid_lat:
            latitude_base = np.ones(len(grid_lon)) * float(lat)
            particles_longitude = np.append(particles_longitude, grid_lon, axis=0)
            particles_latitude = np.append(particles_latitude, latitude_base, axis=0)

        if depth is not None:
            particles_depth = np.tile(grid[depth], len(particles_longitude))
        else:
            particles_depth = np.zeros(np.shape(particles_latitude))

        if strategy == "4NN2D":
            shape = (len(grid_lat), len(grid_lon))
            particles_associations = cls.associated_4NN2D(
                particles_latitude, particles_longitude, shape
            )
        elif strategy == "8NN2D":
            shape = (len(grid_lat), len(grid_lon))
            particles_associations = cls.associated_8NN2D(
                particles_latitude, particles_longitude, shape
            )

        if land_mask is not None:
            particles = cls.filter_on_land(
                (particles_latitude, particles_longitude, particles_depth),
                land_mask,
                land_mask_coords,
                return_filter=True,
            )
            particles_latitude = particles[0]
            particles_longitude = particles[1]
            particles_depth = particles[2]

            particles_associations = particles_associations[~particles[3]]

            mapping = {-1: -1}
            counter = 0
            for i, p_onland in enumerate(particles[3]):
                if not p_onland:
                    mapping[i] = counter
                    counter += 1
                else:
                    mapping[i] = -1

            k = np.array(list(mapping.keys()))
            v = np.array(list(mapping.values()))

            sidx = k.argsort()

            k = k[sidx]
            v = v[sidx]

            idx = np.searchsorted(k, particles_associations.ravel())
            idx = idx.reshape(particles_associations.shape)
            mask = k[idx] == particles_associations
            particles_associations = np.where(mask, v[idx], particles_associations)
            np.flip(np.sort(particles_associations, axis=1), axis=1)

        return (
            particles_latitude,
            particles_longitude,
            particles_depth,
            particles_associations,
        )
