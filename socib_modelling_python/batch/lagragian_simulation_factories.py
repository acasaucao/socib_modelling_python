# -*- coding: UTF-8 -*-

"""Module with factories to constuct lagragian simulations."""

from .factories.parcels_factory import ParcelsFactory


class SimulationFactory(object):
    """Abstract Factory for Lagrangian Simulations Factories."""

    @classmethod
    def produce(self, specs):
        """Produce a lagragian simulation."""
        kind = specs.get("kind", "parcels")
        if kind == "parcels":
            return ParcelsFactory.produce(specs)
        return None
