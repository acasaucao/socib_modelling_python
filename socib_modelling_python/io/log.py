# -*- coding: UTF-8 -*-

"""Module to control logging."""

import logging.config
from singleton_decorator import singleton

from ..config import Config as MFF_Config

_MFF_CONFIG = MFF_Config()


@singleton
class MFFLog:
    """MFF Log control class."""

    def __init__(self):
        """Initialize Logging."""
        logging.basicConfig(level="INFO")
        config_dict = _MFF_CONFIG.get("logging")
        logging.config.dictConfig(config_dict["logging"])

    def getLogger(self, log_name):
        """Return a logger."""
        return logging.getLogger(log_name)
