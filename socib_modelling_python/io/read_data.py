# -*- coding: UTF-8 -*-

"""Script defining I/O functions using to read data locally or remotely."""

import xarray as xr
import magic
import gzip
import shutil
from dask.diagnostics import ProgressBar

from socib_modelling_python import utils


def read_ncdf_file(filename, *args, **kwargs):
    """
    Read NetCDF files.

    Currently, it uses the Xarray's 'open_dataset' function to open the data.

    Parameters
    ----------
    filename : str
       The path for the netcdf file.
    *args
        The variable arguments are propagated to Xarray's open_dataset.
    **kwargs
        The keyword arguments are propagated to Xarray's open_dataset.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset.
    """
    with ProgressBar():
        return xr.open_dataset(filename, *args, **kwargs)


def read_grib_file(filename, *args, **kwargs):
    """
    Read grib files.

    It uses the Xarray's 'open_dataset' function to open the data using the engine "cfgrib".

    Parameters
    ----------
    filename : str
       The path for the GRIB file.
    *args
        The variable arguments are propagated to Xarray's open_dataset.
    **kwargs
        The keyword arguments are propagated to Xarray's open_dataset.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset.
    """
    return xr.open_dataset(filename, engine="cfgrib", *args, **kwargs)


def read_file(filename, file_type=None, *args, **kwargs):
    """
    Generalized function to read local files.

    It uses python package 'magic' to identify what is the file type.
    If it is a GRIB or NetCDF file, the correspondent specialized function is called.
    If the file type was not recognized, the file path is a directory,
    or it is a file path with wildcards;
    The method tries to open the data using Xarray 'open_dataset'.
    In any case, if the files are not found the method warns the user via stdout and return None.

    Parameters
    ----------
    filename : str
       The path for the local file.
    file_type : str, optional
       File type.
    *args
        The variable arguments are propagated to Xarray's open_dataset.
    **kwargs
        The keyword arguments are propagated to Xarray's open_dataset.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset or None if failed.
    """
    data = None

    if not isinstance(filename, str) and file_type is None:
        file_type = type(filename).__name__

    try:
        if file_type is None:
            file_type = magic.from_file(filename)
    except IsADirectoryError:
        filename = "{}/*".format(filename)
    except FileNotFoundError:
        if "*" not in filename:
            print("FileNotFoundError: No such file or directory: {}".format(filename))
            return data

    if file_type is None:
        try:
            if "*" in filename:
                data = xr.open_mfdataset(filename, *args, **kwargs)
            else:
                data = xr.open_dataset(filename, *args, **kwargs)
        except FileNotFoundError:
            print("FileNotFoundError: No such file or directory: {}".format(filename))
            return None
        except OSError as e:
            if str(e) == "no files to open":
                print(
                    "FileNotFoundError: No such file or directory: {}".format(filename)
                )
                return None
            else:
                raise
    elif file_type in [
        "NetCDF Data Format data",
        "Hierarchical Data Format (version 5) data",
        "NetCDF Data Format data (64-bit offset)",
    ]:
        data = read_ncdf_file(filename, *args, **kwargs)
    elif file_type in [
        "Gridded binary (GRIB) version 1",
        "Gridded binary (GRIB) version 2",
    ]:
        data = read_grib_file(filename, *args, **kwargs)
    elif "gzip compressed data" in file_type:
        data = read_gzip_file(filename, *args, **kwargs)
    elif "GzipFile" in file_type:
        kwargs["engine"] = kwargs.get("engine", "h5netcdf")
        data = read_ncdf_file(filename, *args, **kwargs)
    elif file_type == "empty":
        pass

    if data:
        data.attrs["file_format"] = file_type
        data.attrs["filename"] = filename
    return data


def read_remote(source, *args, **kwargs):
    """
    Read remote data.

    Currently, it uses the Xarray's 'open_dataset' function to open the data.
    In case the 'open_dataset' raises an error due to the decoding time process,
    it disables the decoding and tries open again.
    In case of any other exception, the method returns None.

    Parameters
    ----------
    source : str
       String with the path to the remote dataset.
    *args
        The variable arguments are propagated to Xarray's open_dataset.
    **kwargs
        The keyword arguments are propagated to Xarray's open_dataset.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset or None if failed.
    """
    try:
        data = xr.open_dataset(source, *args, **kwargs)
    except ValueError as e:
        if "unable to decode time units" in str(e):
            print("WARNING: {}", str(e))
            print("WARNING: Trying opening with decode_times=False")
            try:
                data = xr.open_dataset(source, decode_times=False, *args, **kwargs)
            except Exception as e:
                print(str(e))
                data = None
        else:
            print(e)
            data = None
    return data


def read_gzip_file(source, *args, **kwargs):
    """
    Read a netcdf data compressed with gzip.

    Currently, it uses the Xarray's 'open_dataset' function to open the data.
    In case the 'open_dataset' raises an error due to the decoding time process,
    it disables the decoding and tries open again.
    In case of any other exception, the method returns None.

    Parameters
    ----------
    source : str
       String with the path to the remote dataset.
    *args
        The variable arguments are propagated to Xarray's open_dataset.
    **kwargs
        The keyword arguments are propagated to Xarray's open_dataset.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset or None if failed.
    """
    try:
        scratch_dir = kwargs.pop("scratch_dir", ":memory:")
        if scratch_dir != ":memory:":
            temp_nc_filename = utils.patterns.get_tmp_file(
                only_name=True, dir=scratch_dir
            )
            temp_nc_filename = f"{temp_nc_filename}.nc"

            with gzip.open(source) as gzip_data:
                with open(temp_nc_filename, "wb") as temp_nc:
                    shutil.copyfileobj(gzip_data, temp_nc)
            data = read(temp_nc_filename, *args, **kwargs)
        else:
            with gzip.open(source) as gzip_data:
                data = read(gzip_data, *args, **kwargs)
    except ValueError as e:
        print(e)
        data = None
    return data


def read(source, *args, **kwargs):
    """
    Define a main function to read datasets.

    If source is a URL it will try to open the data with the specialized function 'read_remote'.
    Otherwise, it will consider the source a local file and will use the
    specialized function 'read_file'.

    Parameters
    ----------
    source : str
       String with the path to the data source.
    *args
        The variable arguments are propagated to the specialized function.
    **kwargs
        The keyword arguments are propagated to the specialized function.

    Returns
    -------
    data : Xarray dataset.
       Returns a Xarray dataset or None if failed.
    """
    if utils.remote.valid_url(source):
        data = read_remote(source, *args, **kwargs)
    else:
        data = read_file(source, *args, **kwargs)
    return data
