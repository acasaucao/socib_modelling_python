# -*- coding: UTF-8 -*-

"""
__init__ file for thredds crawler from https://github.com/ioos/thredds_crawler.
The crawler was modified since the version 1.5.4.
"""

__version__ = '1.5.4'
