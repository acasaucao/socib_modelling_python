# -*- coding: UTF-8 -*-

"""This module has functions to calculate wind properties."""

import numpy as np
import math
import xarray as xr


def compute_ekman_transport(tau_u, tau_v, latitudes, rho=1030):
    """
    Compute the ekman transport.

    Parameters
    ----------
    tau_u : float or array of floats
        U-component of wind stress in [N m :sup:`-2`]
    tau_v : float or array of floats
        V-component of wind stress in [N m :sup:`-2`]
    latitudes : float or array of floats
        Latitude coordinates for the wind stress components
    rho : float or array of floats
        Water density in [kg m :sup:`-3`].

    Returns
    -------
    M_u, M_v : tuple of floats or tuple array of floats
        M_u, M_v are eastward and northward ekman transport components [m :sup: `2` s :sup:`-1`]
    """
    if not isinstance(tau_u, xr.DataArray):
        tau_u = np.asarray(tau_u)

    if not isinstance(tau_v, xr.DataArray):
        tau_v = np.asarray(tau_v)

    if not isinstance(latitudes, xr.DataArray):
        latitudes = np.asarray(latitudes)

    omega = 2 * np.pi / (24 * 3600)
    out_5_degrees = np.abs(latitudes) > 5

    f = np.where(out_5_degrees, 2 * omega * np.sin(np.radians(latitudes)), np.nan)

    M_u = tau_u / (rho * f)
    M_v = -1 * tau_v / (rho * f)

    return M_u, M_v


def compute_wind_stress(U, V, ref_height=10, rho=1.25e-3, tol=1e-2):
    """
    Compute wind stress using the Large and Pond (JPO, 1981) formulation.

    Parameters
    ----------
    U : float or array of floats
        U-component of wind velocity in [m s :sup:`-1`]
    V : float or array of floats
        V-component of wind velocity in [m s :sup:`-1`]
    ref_height : float or array of floats
        Wind sensor height of reference in meters, default: 10 meters
    rho : float
        Air density in [g cm :sup:`-3`].
    tol : float
        Tolerance threshold used on the drag computation (Large and Pond).

    Returns
    -------
    u10, v10, tau_u, tau_v : tuple of floats or tuple array of floats
        u10, v10 are wind velocity components equivalent at 10 m.
        tau_u, tau_v are the eastward and northward wind stress components [10 :sup: `-5` N cm :sup:`-2`)  # noqa
    """
    U = np.asarray(U)
    V = np.asarray(V)

    w = U + (V * 1j)
    v0 = np.abs(w)

    k = 0.41
    rho = np.asarray(rho)

    a1 = (1 / k) * math.log(ref_height / 10.0)
    d1 = 1.0e35 * np.ones_like(U)

    c = np.zeros_like(U)
    cd = np.ones_like(U) * 1.205e-3

    while np.nanmax(np.abs(c - d1)) > tol:
        c = d1
        cd = np.ones_like(U) * 1.205e-3
        ind = c > 11
        cd = np.where(ind, (0.49 + 0.065 * c) * 1.0e-3, cd)
        d1 = v0 / (1 + a1 * np.sqrt(cd))

    zero_values = (np.abs(U) + np.abs(V)) == 0
    nan_values = np.isnan(U) | np.isnan(V)
    valid_values = ~zero_values & ~nan_values

    t = rho * cd * d1 * 1.0e4
    w10 = np.divide(d1, v0, where=valid_values) * w

    u10 = np.where(zero_values, 0, np.nan)
    v10 = np.where(zero_values, 0, np.nan)
    tau_u = np.where(zero_values, 0, np.nan)
    tau_v = np.where(zero_values, 0, np.nan)

    u10 = np.where(valid_values, np.real(w10), u10)
    v10 = np.where(valid_values, np.imag(w10), v10)
    tau_u = np.where(valid_values, t * u10, tau_u)
    tau_v = np.where(valid_values, t * v10, tau_v)

    return u10, v10, tau_u, tau_v
