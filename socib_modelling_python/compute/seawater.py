# -*- coding: UTF-8 -*-

"""Module with methods to compute seawater properties.

Algorithms on this section are based on:
Algorithms for the computation of fundamental properties of seawater.
doi: http://dx.doi.org/10.25607/OBP-1450
"""

import numpy as np


def press2depth(pressures, latitudes):
    """Convert pressure into depth.

    DEPTH IN METERS FROM PRESSURE IN DECIBARS USING
    SAUNDERS AND FOFONOFF1S METHOD.
    DEEP-SEA RES., 1976,23,109-111.
    FORMULA REFITTED FOR 1980 EQUATION OF STATE
    UNITS:
    PRESSURE P DECIBARS
    LATITUDE LAT DEGREES
    DEPTH DEPTH METERS
    CHECKVALUE: DEPTH = 9712.653 M FOR P=10000 DECIBARS, LATITUDE=30 DEG
    ABOVE FOR STANDARD OCEAN: T=O DEG. CELSIUS; S=35 (PSS~78)
    """
    X = np.sin(np.radians(np.abs(latitudes)))
    X = X * X
    # GR= GRAVITY VARIATION WITH LATITUDE: ANON (1970) BULLETIN GEODESIQUE
    GR = 9.780318 * (1.0 + (5.2788e-3 + 2.36e-5 * X) * X) + 2.184e-6 * 0.5 * pressures
    numerator = (
        ((-1.82e-15 * pressures + 2.279e-10) * pressures - 2.2512e-5) * pressures
        + 9.72659
    ) * pressures

    depth = numerator/GR

    return depth
