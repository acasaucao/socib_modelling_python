# -*- coding: UTF-8 -*-

"""
This module has functions to calculate lagrangian properties.

Lagrangian module to compute properties such as FSLE and FTLE.
"""

import numpy as np
import xarray as xr
from joblib import Parallel, delayed
from tqdm import tqdm
from numba import jit
from ..utils import geodesic as mff_geodesic


@jit(nopython=True)
def calculate_index_separation_distance(trajectory_a, trajectory_b, separation=100_000):
    """
    Calculate temporal distance (in terms of iterations) between two particles.

    Parameters
    ----------
    trajectory_a : array of tuples (float)
        Trajectory of a particle A represented by a array of tuples (latitude, longitude, depth).
    trajectory_b : array of tuples (float)
        Trajectory of a particle B represented by a array of tuples (latitude, longitude, depth).
    separation : float
        Separatioin threshold used to calculate the temporal distance.

    Returns
    -------
    idx : int
        Iteration number where particle A is apart of particle B by the separation threshold.
        If this threshold is not reached returns -1.
    """
    idx = 0
    last_a = np.ones(3, dtype=np.float32) * np.float32(np.nan)
    last_b = np.ones(3, dtype=np.float32) * np.float32(np.nan)
    for a, b in zip(trajectory_a, trajectory_b):
        a = np.array(a)
        b = np.array(b)

        if np.isnan(a).any() and np.isnan(b).any():  # two particles on land
            break

        # updating the last positions
        if not np.isnan(a).any():
            last_a = a
        if not np.isnan(b).any():
            last_b = b

        # one of the particles started on land
        if np.isnan(last_a).any() or np.isnan(last_b).any():
            break

        dis = mff_geodesic.haversine_distance(last_a, last_b)
        if dis >= separation:
            return idx
        idx += 1
    return -1


@jit(nopython=True)
def calculated_neighbours_time_distance(
    pid,
    lats,
    lons,
    deps,
    dt,
    neighbours,
    separation=100_000,
    aggregation="mean",
):
    """
    Calculate the mean temporal distance to a particle separates from its neighbours.

    Parameters
    ----------
    pid : int
        Id from the particle of reference.
    lats : 2d array of floats (particles x time)
        Matrix with latitute positions of all particles during the simulation.
    lons : 2d array of floats (particles x time)
        Matrix with longitude positions of all particles during the simulation.
    deps : 2d array of floats (particles x time)
        Matrix with the depth of all particles during the simulation.
    dt : float
        Time step of each iteration in the trajectories' file.
        It is used to calculate the time of separation, multiplying it by the number of iterations
        required for two particles be more than dealta_f apart from each other.
    neighbours : 2d array of int.
        Matrix indicating the neighbours' id of the particle (n_particles x n_neighbours).
    separation : float
        Separatioin threshold used to calculate the temporal distance.
    aggregation : str
        Aggregation method used to calculate the final FSLE value.
        It can be either 'mean' or 'max'.

    Returns
    -------
    time_distance : float
        Mean temporal distance to a particle separates from its neighbours.
    """
    traj_p = list(zip(lats[pid], lons[pid], deps[pid]))
    results = np.zeros(len(neighbours[pid]))

    for i, nid in enumerate(neighbours[pid]):
        if nid < 0:  # end of the valid neighbours
            break
        traj_n = list(zip(lats[nid], lons[nid], deps[pid]))
        idx = calculate_index_separation_distance(traj_p, traj_n, separation=separation)
        if idx > 0:
            results[i] = idx + 1
        else:
            results[i] = np.nan

    #  A FSLE maximization is equivalent to a minimization of the time distance
    if aggregation == "mean":
        time_distance = np.nanmean(results) * dt
    elif aggregation == "max":
        time_distance = np.nanmin(results) * dt
    elif aggregation == "min":
        time_distance = np.nanmax(results) * dt
    else:
        time_distance = np.nan

    return time_distance


def calculate_FSLE(
    filename_trajectories,
    filename_associations=None,
    n_neighbours=4,
    delta_i=2_000,
    delta_f=100_000,
    delta_t=60,
    latitude="lat",
    longitude="lon",
    depth=None,
    particles="traj",
    aggregation="mean",
    n_jobs=-1,
):
    """
    Calculate the Finite-Size Lyapunov Exponent for a Particles Simulations.

    Parameters
    ----------
    filename : str
        Netcdf file with the trajectories of particles.
    n_neighbours : int
        Number of particles in the neighhood used to calculate the FSLE.
    delta_i : float
        Initial separation of the particles in meters.
    delta_f : float
        Final separation of the particles in meters.
    delta_t : float
        Time step of each iteration in the trajectories' file.
        It is used to calculate the time of separation, multiplying it by the number of iterations
        required for two particles be more than dealta_f apart from each other.
    latitude : str
        Name of the latitude dataset in the trajectories' file.
    longitude : str
        Name of the longitude dataset in the trajectories' file.
    depth : str
        Name of the depth dataset in the trajectories' file.
    particles : str
        Name of the particles' id dataset in the trajectories' file.
    aggregation : str
        Aggregation method used to calculate the final FSLE value.
        It can be either 'mean' or 'max'.
    n_jobs : int
        Number of parrallel process to dispatch.
        Whe n_jobs equal -1, the number of jobs are equal to the number of cpus cores.

    Returns
    -------
    fsle : array of floats
        Finite-size Lyapunov exponent for all particles.
    """
    data = xr.open_dataset(filename_trajectories)
    if filename_associations is not None:
        associations = xr.open_dataset(filename_associations)
        neighbours = associations["associations"].values.astype(int)
        n_particles = associations.dims["particle"]
    else:
        neighbours = None
        for i in range(n_neighbours):
            N = data[f"neighbour_{i}"]
            if neighbours is None:
                neighbours = N.values
            else:
                neighbours = np.column_stack((neighbours, N.values))
        neighbours = neighbours.astype(int)
        n_particles = data.dims["traj"]

    lats = data[latitude].values
    lons = data[longitude].values
    if depth is not None:
        deps = data[depth].values
    else:
        deps = np.zeros_like(lats)

    time_distance = Parallel(n_jobs=n_jobs)(
        delayed(calculated_neighbours_time_distance)(
            i,
            lats,
            lons,
            deps,
            delta_t,
            neighbours,
            separation=delta_f,
            aggregation=aggregation,
        )
        for i in tqdm(range(n_particles))
    )

    fsle = FSLE(delta_i, delta_f, time_distance)

    return fsle


@jit(nopython=True)
def FSLE(delta_i, delta_f, tau):
    """
    Equation of the Finite-Size Lyapunov Exponent.

    Parameters
    ----------
    delta_i : float
        Initial spatial separation.
    delta_f : float
        Final spatial separation.
    tau : float
        Time past until delta_f is reached.

    Returns
    -------
    fsle_lambda : float
        Finite-size Lyapunov exponent (lambda).
    """
    fsle_lambda = 0
    di = np.array(delta_i)
    df = np.array(delta_f)
    t = np.array(tau)

    fsle_lambda = 1 / t * np.log(df / di)

    return fsle_lambda
