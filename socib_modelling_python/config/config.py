# -*- coding: UTF-8 -*-

"""This module has functions read/write and control configuration files."""

import json
import os


class Config:
    """Config class to manager all configurations."""

    _MY_PATH = os.path.dirname(__file__)
    _config = {}
    _config_list = [
        ("miner", "db/miner_socib.json"),
        ("regions", "db/geographic_regions.json"),
        ("ocean_parcels_3d", "db/ocean_parcels_3d.json"),
        ("logging", "db/logging.json"),
    ]
    for config_name, filename in _config_list:
        if not filename.startswith("/"):
            filename = "{}/{}".format(_MY_PATH, filename)
        with open(filename, "r") as config_file:
            _config[config_name] = json.load(config_file)

    def __init__(self):
        """Initialize class."""

    def load_config(self, config_name, filename):
        """
        Load a configuration file (json format).

        Parameters
        ----------
        config_name : str
           The configuration name.
        filename : str
           The path for a configuration file describing the products.
        """
        if not filename.startswith("/"):
            filename = "{}/{}".format(self.MY_PATH, filename)

        with open(filename, "r") as config_file:
            Config._config[config_name] = json.load(config_file)

    @classmethod
    def get(cls, config_name):
        """
        Read the configuration file (json format).

        Parameters
        ----------
        config_name : str
           The configuration name.

        Returns
        -------
        configuration : dict
           Returns a dictionary with the configuration
           or a empty dictionary if the configuration does not exist.
        """
        return cls._config.get(config_name, {})
