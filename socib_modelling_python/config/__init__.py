# -*- coding: UTF-8 -*-

"""Configuration module: __init__ file."""

from .config import Config

__all__ = ["Config"]
