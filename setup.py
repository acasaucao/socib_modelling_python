# -*- coding: UTF-8 -*-

"""Install socib_modelling_python and dependencies."""

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages


setup(
    name='socib_modelling_python',
    description='Python Utilities from SOCIB Modelling and Forecasting Facility.',
    version='0.2dev',
    author='socib.es MFF team',
    packages=find_packages(),
    license='MIT License',
    long_description=open('README.md').read(),
    include_package_data=True
)
