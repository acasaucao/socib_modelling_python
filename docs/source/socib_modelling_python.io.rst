socib\_modelling\_python.io package
===================================

Submodules
----------

socib\_modelling\_python.io.read\_data module
---------------------------------------------

.. automodule:: socib_modelling_python.io.read_data
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: socib_modelling_python.io
   :members:
   :undoc-members:
   :show-inheritance:
