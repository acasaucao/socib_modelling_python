socib\_modelling\_python.miner package
======================================

Submodules
----------

socib\_modelling\_python.miner.miner module
-------------------------------------------

.. automodule:: socib_modelling_python.miner.miner
   :members:
   :undoc-members:
   :show-inheritance:

socib\_modelling\_python.miner.miner\_socib module
--------------------------------------------------

.. automodule:: socib_modelling_python.miner.miner_socib
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: socib_modelling_python.miner
   :members:
   :undoc-members:
   :show-inheritance:
