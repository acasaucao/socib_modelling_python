.. SOCIB Modelling and Forecasting Facility Tools documentation master file, created by
   sphinx-quickstart on Mon Sep 16 15:52:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SOCIB Modelling and Forecasting Facility Tools's documentation!
==========================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
