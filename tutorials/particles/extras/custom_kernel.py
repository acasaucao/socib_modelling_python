# -*- coding: UTF-8 -*-

"""A simple customized kernel."""


def Sampling_Bathymetry(particle, fieldset, time):
    """Sample bathymetry under the particle."""
    particle.H = fieldset.H[time, particle.depth, particle.lat, particle.lon]
    
def Sampling_Fields(particle, fieldset, time):
    """Sampling fields on particle location."""
    particle.H = fieldset.H[time, particle.depth, particle.lat, particle.lon]
    particle.T = fieldset.T[time, particle.depth, particle.lat, particle.lon]
