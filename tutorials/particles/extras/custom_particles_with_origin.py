from parcels import JITParticle, Variable
import numpy as np


class JITParticleWithOrigin(JITParticle):
    """Particle with a variable to keep a origin information."""

    origin = Variable("origin", dtype=np.float32, to_write="once")
    

class JITParticleBathymetry(JITParticle):
    """Particle sampling one field."""

    H = Variable("H", dtype=np.float32, initial=0)
    

class JITParticleSamplingFields(JITParticle):
    """Particle sampling one field."""

    H = Variable("H", dtype=np.float32, initial=0)
    T = Variable("T", dtype=np.float32, initial=0)
